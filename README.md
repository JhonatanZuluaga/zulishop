# Taller Parcial Elect. Desarrollo Web Avanazado 2017-2
##### Profesor: Pablo Bejarano
##### Fecha de entrega: 4 de Septiembre de 2017
##### El taller preparcial equivale al 10% del corte
##### El examen teórico parcial equivale al 10% del corte
##### El Quiz de Shell, las tareas de servicios y los demás compromisos equivalen al 10% del corte
#
#
El taller será desarrollado y enregado en grupos mediante la adición del docente al repositorio con permisos de Administración.

El taller busca reforzar sus competencias prácticas en desarrollo web, cubriendo temas de programación orientada a objetos, patrónes de diseño, backend web.

## Desarrollo (Parte Práctica)

### La idea - ¿Qué vamos a hacer?

Vamos a desarrollar una tienda en línea más enfocado en el backstage (el panel adminsitrativo) que la misma tienda. una tienda tan genérica que a futuro podría ser un framework para elaborar tiendas en línea.

### Base

Dentro de éste repositorio se encuentra una carpeta llamada Zulishop, que contiene la base de todo el proyecto hasta donde se había trabajado en clase.

## Funcionalidades del sistema

Las funcionalidades:

1. **Bronce** son aquellas que deben estár en el producto final para poder ser viable, si alguna funcionalidad bronce no está, el producto no es calificable.
2. **Plata** son funcionalidades que suman puntos al proyecto y por las cuales usted podra sumer hasta 5 puntos en la parte teórica.
3. **Oro** son funcionalidades bonus que usted podrá hacer de manera opcional, lo que significa que no hacerlas no hará que su parte práctica tenga una nota inferior, pero realizarlas otorgará bonificaciones a la calificación ganada por puntos plata.

**Nota:** Las funcionalidades valen 0, 0.5 ó 1, es decir, nada, la mitad o toda.

#### Bronce

1. Se respetará el patrón arquitectonico MVC y cada elemento estará ubicado en el lugar supuesto para el mismo.
2. Usted teniendo ámplios conocimientos en bases de datos va a desarrollar un modelo de datos orientado a objetos, en donde cada tabla equivale a un modelo del proyecto. Su modelo de datos debe soportar como mínimo:
    + Marcas de productos
    + Productos
    + Categorias de productos
    + Proveedores de productos
    + Clientes
    + Carritos de compra
    + Facturas
    + Usuarios
    + Roles de usuarios
    + Menús
    + Comentarios de los productos
    + Calificación de los productos
    + Descuentos en productos
    + Promociones al estilo Bundles, que permiten comprar más barato un combo de productos siempre que se lleven todos al tiempo.

(Seamos realistas, la mayoría ya los hicimos en clase dentro del modelo de datos).

3. El proyecto debe contener:
    + al menos 4 usuarios de distintos roles.
    + al menos 3 clientes.
    + al menos 5 categorías.
    + al menos 15 productos por categoría.
    + al menos 5 productos con descuento.
    + al menos 4 promociones tipo bundle.
    + al menos 1 tipo de menú para el panel adminsitrativo por cada rol (es decir al menos 4 tipos de menú).
    + al menos 3 facturas de compras generadas.
    + los productos deben estár totalmente diligenciados y al menos 10% de los productos ya están puntuados o tienen comentarios.

**Nota:** Use datos que simulen la realidad, todo producto como: Cubo rubik, que contenga una foto de una lámpara o una foto distorcionada, será anulado.

4. Tanto la tienda como el panel tienen un aspecto comercial, por ende no se aceptarán sitios en construcción ni llenas de textos sin mas. usted podrá usar templates para la tienda si así lo desea como [begge](http://begge.themesun.com/).

5. las transacciones y relaciones entre los elementos se darán usando ORM, por ende no se aceptarán consultas escritas a mano (no automatizadas) para interactuar con la base de datos.

### Plata

### General

1. Implemente adecuadamente los patrones de diseño y respete los actuales dentro del proyecto, lo que le implica crear fabricas de objetos por ejemplo.

2. Todas las contraseñas deben ser encriptadas.

### Panel Administrativo

1. Todas las entidades del sistema deben poder ser gestionadas desde el panel administrativo, lo que quiere decir que todas las entidades podrán ser creadas, editadas, consultadas y borradas desde el panel. Todo ello mediante el uso de formularios orientados a usuarios finales que **no conocen de ids ni códigos y por eso siempre deben poder ver los nombres asociados a las cosas**.

2. Cada rol tendrá asociada una pag. de inicio que contrandrá un dashboard de información relevante al mismo, por ejemplo, si el rol es de vendedor, el dashboard debería mostrar información de las ventas, productos más vendidos, productos por agotarse, etc. Sea creativo y recuerde que según **bronce** al menos deberá tener 4 dashboards.

3. Todas las consultas serán asincronas, en el caso de tener que subir archivos usted podrá hacerlo usando AJAX2 o falseando la petición con iframes ocultos para conservar la percepción del usuario de asincronismo. Asi mismo, todas las transacciones tendrán respuesta explicita, por ejemplo si un producto es creado, eso debe ser notificado en pantalla, si el producto presentó errores en la creación también y estos deben ser adecuados a los usuarios... no se admitirán mensajes a usuarios como " 1049: Unknown database 'bd_inexistente'" y "1146: Table 'kossu.tabla_inexistente' doesn't exist".

4. El orden de los menus en el panel administrativo podrá ser configurado por un SuperAdmin para cada rol.

5. Todos los usuarios pueden hacer cambio de sus datos desde la sección "perfil" del panel administrativo (útil para cambiar contraseñas y actualizar correos.)

### Tienda

1. La tienda cuenta con una página inicial en donde deben aparecer las promociones, los nuevos productos y los bundles, todos ellos destacados sobre los productos en general.

2. La tienda cuenta con buscador de productos que permite realizar la busqueda por marca, categoría y nombre; busquedas que podrán ser ordenadas por:
    + precio (mayor, menor, entre).
    + Comentarios
    + Valoraciones
    + Vendidos

3. El usuario podrá agregar artículos a su carrito, los cuales estarán permanentemente allí hasta que el producto se agote, el usuario los remueva, la cuenta se elimine o se concrete la compra.

4. Al concretar una compra la tienda genera un PDF con la factura para que el usuario la pueda guardar o imprimir.

5. Los usuarios podrán crear cuentas para poder realizar compras y a su vez editar su información desde su perfil.

**Nota:** Se le recomienda pensar en las secciones como templates de información dinámicos, es decir, su tienda debería contemplar las siguientes vistas:
    + Home
    + Search
    + Product
    + ShoppingCart
    + Receipt
    + Profile

Y por ejemplo la vista Product podría recibir con parametros por URL (GET) el id del producto a visualziar y de esta forma este se mostraría.

Recuerde también que se asume que el usuario tiene saldo en su cuenta y no existirán validaciones de ese tipo al comprar, mientras que las validaciones de stock en inventario y descuentos si deben llevarse a cabo.

### Oro (Si usted está en un grupo de 3 personas el Oro es Plata)

1. La tienda es Responsive. (+0.5)
2. La tienda sugiere a los usuarios productos que otros usuarios han comprado cuando compran ese producto, como lo hace amazon. (+0.5).
3. El panel administrativo lleva un registro de todas las acciones realizadas por los usuarios que se logeen para ser monitoreados por los superadmins y admins. (+0.5)

## Entrega

El proyecto debe ser subido a un repositorio en Bitbucket, para esto recuerdo borrar el .git que ya posee el proyecto para poder subirlo como un nuevo proyecto. Todos los proyectos serán revisados en el commit anterior a la hora de clase, es decir, sólo se calificarán proyectos subidos antes de las 6:30 pm del 4 de septiembre de 2017.

Muchos exitos.........