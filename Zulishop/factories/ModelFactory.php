<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ModelFactory
 *
 * @author pabhoz
 */
class ModelFactory implements IModelFactory{
    
  
    public function newAction($id, $action): \Action {
        
    }

    public function newBrand($id, $name, $logo): \Brand {
        
    }

    public function newBundle($id, $discount): \Bundle {
        
    }

    public function newCategory($id, $name, $parent): \Category {
        
    }

    public function newClient($id, $username, $password, $email): \Client {
        
    }

    public function newComment($id, $idClientProduct, $commentary): \Comment {
        
    }

    public function newImagen($id, $src): \Imagen {
        
    }

    public function newMenu($id, $name, $rol): \Menu {
        
    }

    public function newMenuItem($id, $name, $url, $parent): \MenuItem {
        
    }

    public function newProduct($id, $name, $description, $price, $quantity, $brand, $disscount, $rating): \Product {
        
    }

    public function newProvider($id, $name, $tel, $email, $address): \Provider {
        
    }

    public function newRating_c_x_p($idClientProduct, $rating): \Rating_c_x_p {
        
    }

    public function newReceipt($id, $date, $client, $total): \Receipt {
        
    }

    public function newRol($id, $rol): \Rol {
        
    }

    public function newShoppingCar($id, $Product_id, $quantity): \ShoppingCar {
        
    }
    public function newUser($id, $username, $password, $email, $rol): \User {
        
    }

}
