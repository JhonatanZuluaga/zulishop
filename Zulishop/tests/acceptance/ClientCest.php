<?php
use \Codeception\Util\Locator;

class ClientCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function loginTest(AcceptanceTester $I)
    {
        $I->amOnPage('/index.php');
        $I->seeInCurrentUrl('/index.php');
        $I->see('Log in');
        $I->click('Log in');
        $I->seeInCurrentUrl('/login');
        $I->see('Log in');
        $I->fillField(['name' => 'email'], 'kabeto@gmail.com');
        $I->fillField(['name' => 'password'], '321');
        $I->click('Submit');
        $I->see('Bienvenido');
        $I->seeInCurrentUrl('/login');
        $I->amOnPage('/index.php');
        $I->seeInCurrentUrl('/index.php');
        //$I->see('kabeto');
        //$I->seeElement('Log out');
        //\Codeception\Test\Unit::assertEquals("0","0");
        //$I->seeElement('');
    }
    
    public function RegisterTest(AcceptanceTester $I)
    {
        $I->amOnPage('/index.php');
        $I->seeInCurrentUrl('/index.php');
        $I->see('Log in');
        $I->click('Log in');
        $I->seeInCurrentUrl('/login');
        $I->see('Log in');
        $I->click('Crear Cuenta');
        $I->seeInCurrentUrl('/register');
        $I->fillField(['name' => 'username'], 'kael');
        $I->fillField(['name' => 'email'], 'kael@gmail.com');
        $I->fillField(['name' => 'password'], '321');
        $I->fillField(['name' => 'passwordR'], '321');
        $I->click('Submit');
        //$I->click(['id' => 'nicolas']);
        //$I->see('Correctamente Creado');
        $I->seeInCurrentUrl('/register');
        $I->amOnPage('/index.php');
        $I->seeInCurrentUrl('/index.php');
    
    }
}
