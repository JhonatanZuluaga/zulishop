<?php
require "tests/config.php";

class ClientTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testCreate(){
        $client = new Client(null, "Jhon", "1234", "jhon@doe.com");
       
        \Codeception\Util\Debug::debug("Client created: ");
        \Codeception\Util\Debug::debug($client);
     
    }
    
    public function testRead(){
        
        $client = new Client(1, "Jhon", "1234", "jhon@doe.com");
        
        
        $id = $client->getId();
        $username = $client->getUsername();
        $password = $client->getPassword();
        $email = $client->getEmail();
       
        \Codeception\Util\Debug::debug("Cliente read: ");
        \Codeception\Util\Debug::debug($username);
        
    }
    
    public function testUpdate(){
        
        $client = new Client(1, "Jhon", "1234", "jhon@doe.com");
        //$client = Client::getBy("email", "jhon@doe.com");
        
        $client->setUsername("dionisio");
       
        
        $client->setPassword("4321");

        
        $client->setEmail("dionisioo@gmail.com");

        
        $client->setId(100);

        
        //$lclient = Client::getBy("email", "jhon@doe.com");
        
        //\Codeception\Util\Debug::debug("Client Update: ");
        //\Codeception\Util\Debug::debug($lclient);
        
    }
    
    
}