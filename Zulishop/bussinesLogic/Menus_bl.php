<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Menus_bl {
 
 public static function getUserMenus($rol){
     
     $menu = Menu::getBy("rol", $rol);
     if(!isset($menu)){ return []; }
     $menus = Menu::whereR("MenuItem_id", "Menu_id", $menu->getId(), "Menu_x_Item");

     foreach ($menus as $key => $menu) {
         $menus[$key] = MenuItem::getById($menu["MenuItem_id"]);
     }
     
     return $menus;

}
}