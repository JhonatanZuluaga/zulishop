<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Users_bl {

 public static function login($username,$password){
     $usr= User::getBy("username", $username);
     if(!is_null($usr)){
        return ($usr->getPassword() == $password);
     }
     return false;
 }
 
 public static function getUser($id){
     $usr = User::getById($id);
     if(isset($usr)){
     $usr->rolDetail = Rol::getById($usr->getRol());
     return $usr;
     }else{
         return false;
     }
 }
 public static function getUserPro($id){
     if(isset($id)){
     $usr = User::getById($id);
   
     return $usr;
     }else{
         return false;
     }
 }
 
 public static function getAll(){
     $users = User::getAll();
     foreach ( $users as $n => $user){
         $users[$n] = self::getUser($user["id"]);
     }
     return $users;
 }

 public static function getByUsername($username){
    return User::getBy("username",$username);
 }
 
 public static function eliminar(User $usr){
     if(!is_null($usr)){
         $usr->delete();
        return true;
     }
     return false;
 }
 
  public static function actualizar(User $usr){
     if(!is_null($usr)){
         $usr->update();
        return true;
     }
     return false;
 }
 
  public static function create($data) {

        if (isset($data)) {
            User::instanciate($data)->create();
            return true;
        } else {
            return false;
        }
    }

}
