<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Brands_bl {
 
 public static function getAll(){
     return Brand::getAll();
 }
 
  public static function create($data){
    if(isset($data)){
        Brand::instanciate($data)->create();
     return true;
     }else{
         return false;
     }
 }

  public static function subirFoto($archivo){
      $carpeta='public/dist/img/brands/' ;
//      print_r($archivo);
    $urlImg = Image::upload($carpeta,"logo");
    return $urlImg;
     
 }

 public static function eliminar(Brand $brand){
    if(!is_null($brand)){
        $brand->delete();
       return true;
    }
    return false;
}

 public static function actualizar(Brand $brand){
    if(!is_null($brand)){
        $brand->update();
       return true;
    }
    return false;
}

}
