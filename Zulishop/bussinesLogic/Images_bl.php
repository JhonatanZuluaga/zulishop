<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Images_bl {
 
 public static function getAll(){
     return Imagen::getAll();
 }
  public static function getMyProduct(){
     return Product::getAll();
 }
 public static function getImage($id){
     $pro = Imagen::getById($id); 
     if(isset($pro)){
     $pro->nombreMarca = Product::getById($pro->getId());
     return $pro;
     }else{
         return false;
     }
 }
 
  public static function getAll2(){
     $products = Imagen::getAll();
     foreach ( $products as $n => $product){
         $products[$n] = self::getImage($product["id"]);
     }
     return $products;
 }
// 
  public static function create($data){
    if(isset($data)){
        Imagen::instanciate($data)->create();
     return true;
     }else{
         return false;
     }
 }

  public static function subirFoto($archivo){
      $carpeta='public/dist/img/products/' ;
//      print_r($archivo);
    $urlImg = Image::upload($carpeta,"src");
    return $urlImg;
     
 }

}
