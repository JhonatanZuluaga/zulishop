<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Rols_bl {

    public static function create($data) {

        if (isset($data)) {
            $obj = new Rol(null, $data["rol"]);
            $obj->create();
            return true;
        } else {
            return false;
        }
    }

    public static function getAll() {
        return Rol::getAll();
    }

    
    public static function eliminar(Rol $rol){
        if(!is_null($rol)){
            $rol->delete();
           return true;
        }
        return false;
    }
    
     public static function actualizar(Rol $rol){
        if(!is_null($rol)){
            $rol->update();
           return true;
        }
        return false;
    }
    
//      public static function traking() {
//         $id = Session::get("aid");
//         $usr = User::getById($id);
//         $acion = Action::getById("1");
//         $data = array(
//                'Ently' => '1',
//                'idElement' => '2'
//                // 'aFloat' => 0.0, 'aString' => '' 
//            );
//         $usr->has_many("actionUser",$acion);
//         $t = $usr->update();
//         echo $t ;
//    }


}
