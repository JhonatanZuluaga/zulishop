<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Bundles_bl {
 
 public static function getAll(){
     return Bundle::getAll();
 }

   public static function create($data) {

        if (isset($data)) {
            $obj = new Bundle(null, $data["discount"]);
            $obj->create();
            return true;
        } else {
            return false;
        }
    }
}
