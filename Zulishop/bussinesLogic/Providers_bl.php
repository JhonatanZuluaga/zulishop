<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Providers_bl {
 
 public static function getAll(){
     return Provider::getAll();
 }
 
  public static function create($data){
    if(isset($data)){
        Provider::instanciate($data)->create();
     return true;
     }else{
         return false;
     }
 }

 public static function actualizar(Provider $prov){
    if(!is_null($prov)){
        $prov->update();
       return true;
    }
    return false;
}

public static function eliminar(Provider $prov){
    if(!is_null($prov)){
        $prov->delete();
       return true;
    }
    return false;
}

}
