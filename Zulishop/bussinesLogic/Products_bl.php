<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Products_bl {
    
     public static function getProduct($id){
     $pro = Product::getById($id);
     if(isset($pro)){
     $pro->marcaDetail = Brand::getById($pro->getBrand());
     return $pro;
     }else{
         return false;
     }
    }
    
    public static function create($data){
        if(isset($data)){
            
            if($data["brand"]== "null"){
                return false;
            }else{
        Product::instanciate($data)->create();
            }
        return true;
        }else{
            return false;
        }
    }

    
    public static function getAll2(){
        $products = Product::getAll();
        foreach ( $products as $n => $product){
            $products[$n] = self::getProduct($product["id"]);
        }
        return $products;
    }

    
    public static function getAll(){
        return Product::getAll();
    }
 

 
    public static function eliminar(Product $prod){
        if(!is_null($prod)){
            $prod->delete();
            return true;
        }
        return false;
    }

    public static function actualizar(Product $prod){
        if(!is_null($prod)){
            $prod->update();
        return true;
        }
        return false;
    }
    public static function getProductBy($id){
        return Product::getBy("id", $id);
        
    }
}
