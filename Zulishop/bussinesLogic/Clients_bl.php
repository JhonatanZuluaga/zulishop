<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Clients_bl {

//    public static function login($username, $password) {
//        $usr = Client::getBy("username", $username);
//        if (!is_null($usr)) {
//            return ($usr->getPassword() == $password);
//        }
//        return false;
//    }
    
      public static function login(Client $usr, $password) {
          //print_r($usr->getPassword());
        $cryPass = self::cripMypass($password);

        if ($usr->getPassword() == $cryPass) {
            $res = 0;
//            self::crearSesion($usr);
        } else {
            $res = 1;
        }
        return $res;
    }

    public static function getClient($id) {
        $usr = Client::getById($id);
        if (isset($usr)) {
            //$usr->rolDetail = Rol::getById($usr->getRol());
            return $usr;
        } else {
            return false;
        }
    }

    public static function getAll() {
        $users = Client::getAll();

        return $users;
    }

    public static function getByUsername($username) {
        return Client::getBy("username", $username);
    }

    public static function eliminar(Client $cliente){
        if(!is_null($cliente)){
            $cliente->delete();
           return true;
        }
        return false;
    }
    
     public static function actualizar(Client $client){
        if(!is_null($client)){
            $client->update();
           return true;
        }
        return false;
    }    

    public static function registroClient($data) {
        $cryPass = self::cripMypass($data["password"]);
        $data["password"] = $cryPass;
        $usr = Client::instanciate($data);
        $r = $usr->create();
        if ($r["error"] == 0) {
            return $usr;
        } else {
            return $r["error"];
        }
    }

    public static function cripMypass($pass) {
        $cryp = Hash::create($pass);

        return $cryp;
    }
    
    public static function compararPassword($password1, $password2){
        
        $cryPass = self::cripMypass($password2);
        if($cryPass == $password1){
            $r = 1;
            return $r;
        }else{
            $r = 0;
            return $r;
            
        }
        
    }

  

}
