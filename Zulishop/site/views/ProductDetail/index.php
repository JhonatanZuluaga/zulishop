<?php $this->loadModule("head"); ?>
<body>
    <div class="body">
        <?php $this->loadModule("header"); ?>
        <?php $this->loadModule("mobile-nav"); ?>
        <div role="main" class="main">
            <!-- breadcrumb -->
            <section class="page-header">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="#">Home</a></li>

                            <li><a href="#">Fashion</a></li>
                            <li class="active">Black Maxi Dress</li>
                        </ul>
                    </div>
            </section>

            <div class="container">
				<div class="row">
					<div class="col-md-9">
                        <?php $this->loadModule("product-info"); ?>                           
                        <?php $this->loadModule("similar-products"); ?>                           
                    </div>
                    <?php $this->loadModule("aside-productDetail"); ?>                                               
                </div>
            </div>

        </div>
        <?php $this->loadModule("footer"); ?>                        
    </div>
    <?php $this->loadModule("ux-scripts"); ?>                
</body>
</html>