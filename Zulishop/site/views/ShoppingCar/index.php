<?php $this->loadModule("head"); ?>
<body>
    <div class="body">
        <?php $this->loadModule("header"); ?>
        <?php $this->loadModule("mobile-nav"); ?>
        <div role="main" class="main">
            <?php $this->loadModule("car"); ?>                                               
        </div>
        <?php $this->loadModule("footer"); ?>                        
    </div>
    <?php $this->loadModule("ux-scripts"); ?>                
</body>
</html>