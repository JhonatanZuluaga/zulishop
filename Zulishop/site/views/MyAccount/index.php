<?php $this->loadModule("head"); ?>
<body>
    <div class="body">
        <?php $this->loadModule("header"); ?>
        <?php $this->loadModule("mobile-nav"); ?>
        <div role="main" class="main">
            <section class="page-header">
				<div class="container">
					<ul class="breadcrumb">
						<li><a href="#">Home</a></li>

						<li class="active">Blog</li>
					</ul>
				</div>
			</section>
            <?php $this->loadModule("myAccount-form"); ?>                
                        
        </div>
    </div>
    <?php $this->loadModule("ux-scripts"); ?>                
</body>
</html>