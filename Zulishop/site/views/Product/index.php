<?php $this->loadModule("head"); ?>
<body>
    <div class="body">
        <?php $this->loadModule("header"); ?>
        <?php $this->loadModule("mobile-nav"); ?>
        <div role="main" class="main">
            <!-- breadcrumb -->
            <section class="page-header">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="#">Home</a></li>

                            <li><a href="#">Fashion</a></li>
                            <li class="active">Black Maxi Dress</li>
                        </ul>
                    </div>
            </section>

			<div class="container">
                <div class="row">
                    <div class="col-md-9 col-md-push-3">
                        <?php $this->loadModule("product-toolbar"); ?>                        					                                         
                        <?php $this->loadModule("products-grid"); ?>                        					                                         
                        <?php $this->loadModule("product-toolbar-bottom"); ?>                        					                                         
                    </div>
                    <?php $this->loadModule("products-aside"); ?>                        					                                                             
                </div>
            </div>

        </div>
        <?php $this->loadModule("footer"); ?>                        
    </div>
    <script src="<?php print(URL); ?>public/frameworks/nouislider/nouislider.min.js"></script>                
    <?php $this->loadModule("ux-scripts"); ?>
</body>
</html>