<div class="toolbar mb-none">
    <div class="sorter">
        <div class="sort-by">
            <label>Sort by:</label>
            <select>
                <option value="Position">Position</option>
                <option value="Name">Name</option>
                <option value="Price">Price</option>
            </select>
        </div>

        <div class="view-mode">
            <span title="Grid">
                <i class="fa fa-th"></i>
            </span>
            <a href="#" title="List">
                <i class="fa fa-list-ul"></i>
            </a>
        </div>

        <ul class="pagination">
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#"><i class="fa fa-caret-right"></i></a></li>
        </ul>

        <div class="limiter">
            <label>Show:</label>
            <select>
                <option value="12">12</option>
                <option value="24">24</option>
                <option value="36">36</option>
            </select>
        </div>
    </div>
</div>