<aside class="col-md-3 sidebar product-sidebar">
    <div class="feature-box feature-box-style-3">
        <div class="feature-box-icon">
            <i class="fa fa-truck"></i>
        </div>
        <div class="feature-box-info">
            <h4>FREE SHIPPING</h4>
            <p class="mb-none">Free shipping on all orders over $99.</p>
        </div>
    </div>

    <div class="feature-box feature-box-style-3">
        <div class="feature-box-icon">
            <i class="fa fa-dollar"></i>
        </div>
        <div class="feature-box-info">
            <h4>MONEY BACK GUARANTEE</h4>
            <p class="mb-none">100% money back guarantee.</p>
        </div>
    </div>

    <div class="feature-box feature-box-style-3">
        <div class="feature-box-icon">
            <i class="fa fa-support"></i>
        </div>
        <div class="feature-box-info">
            <h4>ONLINE SUPPORT 24/7</h4>
            <p class="mb-none">Lorem ipsum dolor sit amet.</p>
        </div>
    </div>

    <hr class="mt-xlg">

    <div class="owl-carousel owl-theme" data-plugin-options="{'items':1, 'margin': 5}">
        <a href="#">
            <img class="img-responsive" src="<?php print(URL); ?>public/dist/img/banners/banner1-black.jpg" alt="Banner">
        </a>
        <a href="#">
            <img class="img-responsive" src="<?php print(URL); ?>public/dist/img/banners/banner2-black.jpg" alt="Banner">
        </a>
    </div>

    <hr>
</aside>