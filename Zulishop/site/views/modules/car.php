<div class="cart">
    <div class="container">
        <h1 class="h2 heading-primary mt-lg clearfix">
            <span>Carrito de Compras</span>
            <a href="#" class="btn btn-primary pull-right">Proceder para Checkout</a>
        </h1>

        <div class="row">
            <div class="col-md-8 col-lg-9">
                <div class="cart-table-wrap">
                    <table class="cart-table">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>Nombre del Producto</th>
                                <th>Precio unitario</th>
                                <th>Cantidad</th>
                                <th>Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>


                            <?php foreach ($this->mycar as $imagen) : ?>
                                <?php foreach ($this->imagenesCool as $imagenes) : ?>
                                    <?php if ($imagen->getId() == $imagenes->getId()) : ?>
                                        <tr>
                                            <td class="product-action-td eliminare" id="<?php print $imagen->getId(); ?>">
                                                <a href="#" title="Eliminar Producto" class="btn-remove"><i class="fa fa-times"></i></a>
                                            </td>
                                            <td class="product-image-td">
                                                <a href="#" title="Nombre del Producto">
                                                    <img src="<?php print(URL); ?>public/dist/img/products/<?php print $imagenes->getSrc(); ?>" alt="Product Name">
                                                </a>
                                            </td>
                                            <td class="product-name-td">
                                                <h2 class="product-name"><a href="#" title="Product Name"><?php print $imagen->getName(); ?></a></h2>
                                            </td>
                                            <td><?php print "$" . $imagen->getPrice(); ?></td>
                                            <td>
                                                <div class="qty-holder">
                                                    <a href="#" class="qty-dec-btn" title="Dec">-</a>
                                                    <input type="text" class="qty-input" value="1">
                                                    <a href="#" class="qty-inc-btn" title="Inc">+</a>
                                                    <a href="#" class="edit-qty"><i class="fa fa-pencil"></i></a>
                                                </div>
                                            </td>
                                            <td>
                                                <span class="text-primary"><?php print "$" . $imagen->getPrice(); ?></span>
                                            </td>
                                        </tr>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="6" class="clearfix">
                                    <button class="btn btn-default hover-primary btn-continue comprar">Comprar</button>
                                    <button class="btn btn-default hover-primary btn-clear limpiar">Limpiar Carrito</button>
                                </td>
                            </tr> 
                        </tfoot>	
                    </table>
                </div>
            </div>
            <aside class="col-md-4 col-lg-3 sidebar shop-sidebar">
                <div class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" href="#panel-cart-total">
                                    Compra Total
                                </a>
                            </h4>
                        </div>
                        <div id="panel-cart-total" class="accordion-body collapse in">
                            <div class="panel-body">
                                <table class="totals-table">
                                    <tbody>
                                        <?php foreach ($this->mycar as $imagen) : ?>
                                            <tr>
                                                <td>Subtotal</td>

                                                <td><?php print "$" . $imagen->getPrice(); ?></td>

                                            </tr>
                                        <?php endforeach; ?>
                                        <tr>
                                            <td>Grand Total</td>
                                            <td><?php print "$" . $this->total; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="totals-table-action">
                                    <a href="#" class="btn btn-primary btn-block">Hacer Checkout</a>
                                    <a href="#" class="btn btn-link btn-block">Checkout con multiples Direcciones</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
        </div>
            
        <div class="crosssell-products">
            <h2 class="h4"><strong>Based on your selection, you may be interested in the following items:</strong></h2>
            <div class="row">
                <?php foreach ($this->rendom as $imagen) : ?>
                <?php foreach ($this->imge as $imagenes) : ?>
                <?php if ($imagen["id"] == $imagenes["id"]) : ?>
                <div class="col-sm-6 col-md-3">
                    <div class="product product-sm">
                        <figure class="product-image-area">
                            <a href="#" title="Product Name" class="product-image">
                                <img src="<?php print(URL); ?>public/dist/img/products/<?php print $imagenes["src"]; ?>" alt="Product Name">
                            </a>
                        </figure>
                        <div class="product-details-area">
                            <h2 class="product-name"><a href="#" title="Product Name"><?php print $imagen["name"]; ?></a></h2>

                            <div class="product-price-box">
                                <span class="old-price"> <?php print $imagen["price"]; ?></span>
                                <span class="product-price"><?php if ($imagen["disscount"] != 0) 
                                    {  print $imagen["price"]-$imagen["disscount"] ;        
                                    }else{ print $imagen["price"];} ?></span>
                            </div>

                            <a href="#" class="btn btn-default hover-primary">Add to Cart</a>
                        </div>
                    </div>
                </div>
                   <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                <div class="clearfix visible-sm"></div>
         
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">


//iniciamos la asignación de listeners

    addToCarrito();
//Función nueva! :D

    function addToCarrito() {
        //capturar los elementos
        var elementos = document.querySelectorAll(".eliminare");
        console.log(elementos);
        for (var i = 0; i < elementos.length; i++) {
            eventodePresion(elementos[i]);
            console.log(elementos);
        }
    }

    function eventodePresion(elemento) {
        console.log("hooli");
        elemento.onclick = function (e) {
            e.preventDefault();
            var data = {};
            var id = this.id;
            data = {
                Product_id: id
            };
            console.log(data);

            swal({
                title: "Esta seguro?",
                text: "Quiere eliminar el producto del Carrito?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, eliminar",
                cancelButtonText: "No, cancelar",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: "<?php print(URL); ?>ShoppingCar/eliminar",
                                method: "POST",
                                data: data
                            }).done(function (r) {
                                console.log(r);
                                //var r = JSON.parse(r);
                                if (r.error) {
                                    alert(r.msg);
                                } else {
                                    
                                     setTimeout(function () {
                                   swal("Eliminado!", "El producto se elimino del carrito", "success");
                                }, 2000, window.location.reload());
                                }
                            });

                        } else {
                            swal("Cancelado", "El producto no se elimino", "error");
                        }
                    });


//      







        }
    }


    $(".comprar").click(function (e) {
        e.preventDefault();

        console.log("noooooo");
        swal({
            title: "Esta seguro?",
            text: "Quiere limpiar el Carrito?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false
        },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({url: "<?php print(URL); ?>ShoppingCar/generarPDF",
                            method: "GET"
                        }).done(function (r) {
//        $("#asyncLoadArea").html(response);
                            if (r.error) {
                                alert(r.msg);
                            } else {
                                
                                setTimeout(function () {
                                    swal("Compra Exitosa!", "Se ha generado su recibo", "success");
                                }, 2000,window.location.reload());
                               
                            }
                        });

                    } else {
                        swal("Cancelado", "No se limpio el Carrito", "error");
                    }
                });





    });
    
     $(".limpiar").click(function (e) {
        e.preventDefault();

        console.log("noooooo");
        swal({
            title: "Esta seguro?",
            text: "Quiere limpiar el Carrito?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false
        },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({url: "<?php print(URL); ?>ShoppingCar/limpiar",
                            method: "GET"
                        }).done(function (r) {
//        $("#asyncLoadArea").html(response);
                            if (r.error) {
                                alert(r.msg);
                            } else {
                                
                                setTimeout(function () {
                                    swal("Eliminado!", "El Carrito se elimino", "success");
                                }, 2000, window.location.reload());
                               
                            }
                        });

                    } else {
                        swal("Cancelado", "No se limpio el Carrito", "error");
                    }
                });





    });
//

    function processForm(form) {
        var inputs = form.querySelectorAll("input");
        var data = {};
        $(inputs).each(function () {
            var input = $(this);
            if (input.context.type != "submit") {
                var attr = input.context.name;
                data[attr] = input.context.value;

            }
        });
        return data;
    }




</script>