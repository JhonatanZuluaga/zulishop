<footer id="footer">
    <div class="footer-copyright">
        <div class="container">
            <a href="index.html" class="logo">
                <img alt="logo" class="img-responsive" src="<?php print(URL); ?>public/assets/images/logo-dark.png">
            </a>
            <ul class="social-icons">
                <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                <li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                <li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
            </ul>
            <img alt="Payments" src="<?php print(URL); ?>public/assets/images/payments.png" class="footer-payment">
            <p class="copyright-text">© Copyright 2017. All Rights Reserved.</p>
        </div>
    </div>
</footer>