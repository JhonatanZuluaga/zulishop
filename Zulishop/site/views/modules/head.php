<!DOCTYPE html>
<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title><?php print($this->title); ?></title>	

		<meta name="keywords" content="TECNOLOGY SHOP" />
		<meta name="description" content="TECNOLOGY SHOP">
		<meta name="author" content="Carlos Cortez, Nicolás Reyes, Jhonatan Zuluaga">

		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php print(URL); ?>public/assets/images/favicon.ico" type="image/x-icon" />
		<!-- <link rel="apple-touch-icon" href="<?php print(URL); ?>public/assets/images/apple-touch-icon.png"> -->

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php print(URL); ?>public/frameworks/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php print(URL); ?>public/frameworks/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php print(URL); ?>public/frameworks/animate/animate.min.css">
		<link rel="stylesheet" href="<?php print(URL); ?>public/frameworks/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="<?php print(URL); ?>public/frameworks/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="<?php print(URL); ?>public/frameworks/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="<?php print(URL); ?>public/frameworks/magnific-popup/magnific-popup.min.css">
		<link rel="stylesheet" href="<?php print(URL); ?>public/frameworks/sweetalert/sweetalert.css">
		<link rel="stylesheet" href="<?php print(URL); ?>public/frameworks/fpdf/fpdf.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php print(URL); ?>public/styles/theme.css">
		<link rel="stylesheet" href="<?php print(URL); ?>public/styles/theme-elements.css">
		<link rel="stylesheet" href="<?php print(URL); ?>public/styles/theme-blog.css">
		<link rel="stylesheet" href="<?php print(URL); ?>public/styles/theme-shop.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="<?php print(URL); ?>public/frameworks/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="<?php print(URL); ?>public/frameworks/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="<?php print(URL); ?>public/frameworks/rs-plugin/css/navigation.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php print(URL); ?>public/styles/skin-shop-2.css"> 

		<!-- Demo CSS -->
		<link rel="stylesheet" href="<?php print(URL); ?>public/styles/demo-shop-2.css">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php print(URL); ?>public/styles/custom.css">

		<!-- Head Libs -->
		<script src="<?php print(URL); ?>public/frameworks/modernizr/modernizr.min.js"></script>
                <script src="<?php print(URL); ?>public/frameworks/jquery/jquery.min.js"></script>

	</head>