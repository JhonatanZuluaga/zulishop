<section class="form-section">
    <div class="container">
        <h1 class="h2 heading-primary font-weight-normal mb-md mt-lg">Ingresa o Crea una Cuenta</h1>

        <div class="featured-box featured-box-primary featured-box-flat featured-box-text-left mt-md">
            <div class="box-content">
                <form id="formularioCool">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-content">
                                <h3 class="heading-text-color font-weight-normal">Nuevos Usuarios ZuliShop</h3>
                                <p>Al crear una cuenta en nuestra tienda, podrá desplazarse más rápidamente por el proceso de pago, almacenar varias direcciones de envío, ver y realizar un seguimiento de sus pedidos en su cuenta y mucho más.</p>
                            </div>

                            <div class="form-action clearfix">
                                <a href="<?php print(URL); ?>register" class="btn btn-primary">Crear Cuenta</a>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-content">
                                <h3 class="heading-text-color font-weight-normal">Clientes Registrados</h3>
                                <p>Si tu tienes una cuenta con nosotros, porfavor ingresa.</p>
                                <div class="form-group">
                                    <label class="font-weight-normal">Email <span class="required">*</span></label>
                                    <input type="text" class="form-control" required name="email">
                                </div>

                                <div class="form-group">
                                    <label class="font-weight-normal">Contraseña <span class="required">*</span></label>
                                    <input type="password" class="form-control" required name="password">
                                </div>

                                <p class="required">* Requiere datos</p>
                            </div>

                            <div class="form-action clearfix">
                                <!-- <a href="#" class="pull-left">Forgot Your Password?</a> -->

                                <input type="submit" class="btn btn-primary" value="Submit" id="nicolas">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>    

<script>
	$("#nicolas").click(function(e){
		e.preventDefault();

		var form = document.querySelector("#formularioCool");
		var data = {}

		
			data = processForm(form);
			console.log(data);

					//Envío asincrono
					$.ajax({
						url:"<?php print(URL);?>Clients/entrar/",
						method:"POST",
						data: data
					}).done(function(r){
						if(JSON.parse(r)){

							var r = JSON.parse(r);
							console.log(r);
							if(r.error == 0){
                                                            alert("Bienvenido");
								window.location.replace("<?php print(URL); ?>Index");
							}else{
								alert("Username or Password incorrect");
							}
						}
						
					});
		
	});
        
        
               function processForm(form) {

        var inputs = form.querySelectorAll("input");
        
        var data = {};
       $(inputs).each(function () {
            var input = $(this);
            
          if (input[0].type != "submit") {
                var attr = input[0].name;
                data[attr] = input[0].value;
                    
          }
       });

        return data;
    }
        
        </script>