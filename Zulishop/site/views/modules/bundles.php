<div class="banners-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <a href="#" class="banner">
                    <img src="<?php print(URL); ?>public/dist/img/Bundles/bundles1.jpg" alt="Banner">
                    <span class="ban-title">Paquete blanco tradicional</span>
                </a>
            </div>
            <div class="col-sm-4">
                <a href="#" class="banner">
                    <img src="<?php print(URL); ?>public/dist/img/Bundles/bundles2.jpg" alt="Banner">
                    <span class="ban-title">Paquete Camaraógrafo pro</span>
                </a>
            </div>
            <div class="col-sm-4">
                <a href="#" class="banner">
                    <img src="<?php print(URL); ?>public/dist/img/Bundles/bundles3.jpg" alt="Banner">
                    <span class="ban-title"> Paquete Mouse</span>
                </a>
            </div>
        </div>
    </div>
</div>