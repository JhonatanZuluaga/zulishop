<section class="form-section register-form">
    <div class="container">
        <h1 class="h2 heading-primary font-weight-normal mb-md mt-lg">Crer una Cuenta</h1>

        <div class="featured-box featured-box-primary featured-box-flat featured-box-text-left mt-md">
            <div class="box-content">
                <form id="formularioCool">

                    <h4 class="heading-primary text-uppercase mb-lg">INFORMACION PERSONAL</h4>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="font-weight-normal">Usuario <span class="required">*</span></label>
                                <input type="text" class="form-control" name="username" required>
                            </div>
                        </div>
                        <!-- </div> -->

                        <!-- <div class="row"> -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="font-weight-normal">Email @ <span class="required">*</span></label>
                                <input type="email" class="form-control" name="email" required>
                            </div>
                        </div>
                    </div>
                    <h4 class="heading-primary text-uppercase mb-lg">INFORMACION PARA LOGIN</h4>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="font-weight-normal">Contraseña <span class="required">*</span></label>
                                <input type="password" class="form-control"  name="password" required>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="font-weight-normal">Confrime Contraseña <span class="required">*</span></label>
                                <input type="password" class="form-control" name="passwordR" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <p class="required mt-lg mb-none">* Rquiere datos</p>

                            <div class="form-action clearfix mt-none">
                                <a href="" class="pull-left"><i class="fa fa-angle-double-left"></i> Volver</a>

                                <input type="submit" class="btn btn-primary" value="Submit" id="nicolas">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript"> 
    
    
    
    var readyToSend = false;
    var errorMsg = "";
	$("#formularioCool input").on("blur keyup keydown",function(e){
		
			
				var form = document.querySelector("#formularioCool");
//                                console.log(form);
				/*** email & username logic***/
				if(e.type == "blur"){
					//console.log("validando usuario y correo");
					var username = form.querySelector("[name=username]");
                       
					validateUnique(username,"Clients","verificarUsername","username");

					var email = form.querySelector("[name=email]");
					validateUnique(email,"Clients","verificarCorreo","email");
				}
				/*** Passwords logic ****/
				var pass = form.querySelector("[name=password]");
				var rPass = form.querySelector("[name=passwordR]");
//                                console.log(pass.value+"   "+rPass.value);

				if( pass.value != '' && rPass.value != ''){
					if(pass.value == rPass.value){
						$(pass).removeClass("incorrecto").addClass("correcto");
						$(rPass).removeClass("incorrecto").addClass("correcto");

						readyToSend = true;

					}else{
						$(pass).removeClass("correcto").addClass("incorrecto");
						$(rPass).removeClass("correcto").addClass("incorrecto");

						readyToSend = false;
						errorMsg = "Los passwords no coinciden";
					}
				}
			
		
	})
        
        function validateUnique(who, controller, method, name) {
   
        if (who.value != '') {
            $.ajax({
                url: "<?php print(URL); ?>" + controller + "/" + method + "/" + who.value,
                method: "GET"
            }).done(function (r) {
                if (r != 0) {
                    $(who).removeClass("correcto").addClass("incorrecto");
                    readyToSend = false;
                    errorMsg = name + " existente en la base de datos";
                } else {
                    $(who).removeClass("incorrecto").addClass("correcto");
                    readyToSend = true;
                }
            });
        }

    }

 
    
    
    	$("#nicolas").click(function(e){
		e.preventDefault();

		var form = document.querySelector("#formularioCool");
		var data = {}

		//Trigger ejecutando un blur en cada input
		var inputs = form.querySelectorAll("input");
		$(inputs).each(function(){
			if($(this).hasClass("incorrecto")){
				readyToSend = false;
			}
		});

		

		if(readyToSend){
			
				data = processForm(form);	
					$.ajax({
						url:"<?php print(URL);?>Clients/registrar/",
						method:"POST",
						data: data
					}).done(function(r){
						if(JSON.parse(r)){
							var r = JSON.parse(r);
							if(r.error == 0){
								alert(r.msg);
								window.location.replace("<?php print(URL); ?>Index");
							}else{
								alert(r.msg);
							}
						}
						
					});
				
			

		}else{
			alert(errorMsg);
		}
		
		//return false;
	});

       function processForm(form) {

        var inputs = form.querySelectorAll("input");
        
        var data = {};
       $(inputs).each(function () {
            var input = $(this);
            
          if (input[0].type != "submit") {
                var attr = input[0].name;
                data[attr] = input[0].value;
                    
          }
       });

        return data;
    }
    

</script>
