<aside class="col-md-3 col-md-pull-9 sidebar shop-sidebar">
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" href="#panel-filter-category">
                        Categories
                    </a>
                </h4>
            </div>
            <div id="panel-filter-category" class="accordion-body collapse in">
                <div class="panel-body">
                    <?php foreach ($this->categoriticas as $category) : ?>
                    <ul>
                        <li><a href="#"><?php print $category->getName(); ?></a></li>
                    </ul>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" href="#panel-filter-price">
                        Price
                    </a>
                </h4>
            </div>
            <div id="panel-filter-price" class="accordion-body collapse in">
                <div class="panel-body">
                    <div class="filter-price">
                        <div id="price-slider"></div>
                        <div class="filter-price-details">
                            <span>from</span>
                            <input type="text" id="price-range-low" class="form-control" placeholder="Min">
                            <span>to</span>
                            <input type="text" id="price-range-high" class="form-control" placeholder="Max">
                            <a href="#" class="btn btn-primary">FILTER</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" href="#panel-filter-rating">
                        Rating
                    </a>
                </h4>
            </div>
            <div id="panel-filter-rating" class="accordion-body collapse in">
                <div class="panel-body">
                    <ul class="configurable-filter-list">
                        <li>
                            <a href="#">1</a>
                        </li>
                        <li>
                            <a href="#">2</a>
                        </li>
                        <li>
                            <a href="#">3</a>
                        </li>
                        <li>
                            <a href="#">XL</a>
                        </li>
                        <li>
                            <a href="#">2XL</a>
                        </li>
                        <li>
                            <a href="#">3XL</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div> -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" href="#panel-filter-brand">
                        Brands
                    </a>
                </h4>
            </div>
            <div id="panel-filter-brand" class="accordion-body collapse in">
                <div class="panel-body">
                    <ul>
                        <li><a href="#">Nike</a></li>
                        <li><a href="#">Adidas</a></li>
                        <li><a href="#">Puma</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" href="#panel-filter-color">
                        color
                    </a>
                </h4>
            </div>
            <div id="panel-filter-color" class="accordion-body collapse in">
                <div class="panel-body">
                    <ul class="configurable-filter-list filter-list-color">
                        <li>
                            <a href="#">
                                <span style="background-color: #000"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span style="background-color: #21284f"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span style="background-color: #272725"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span style="background-color: #006b20"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span style="background-color: #68686a"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span style="background-color: #1736a9"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span style="background-color: #f6edd1"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span style="background-color: #a69172"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span style="background-color: #d8c7a7"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span style="background-color: #fd9904"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span style="background-color: #f56ab8"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span style="background-color: #442937"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span style="background-color: #bd1721"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span style="background-color: #1226ad"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span style="background-color: #cbcbcb"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span style="background-color: #c7b89a"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span style="background-color: #fff"></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div> -->
    </div>

    <!-- <h4>Featured</h4>
    <div class="owl-carousel owl-theme" data-plugin-options="{'items':1, 'margin': 5, 'dots': false, 'nav': true}">
        <div>
            <div class="product product-sm">
                <figure class="product-image-area">
                    <a href="demo-shop-2-product-details.html" title="Product Name" class="product-image">
                        <img src="../img/demos/shop/products/product13.jpg" alt="Product Name">
                        <img src="../img/demos/shop/products/product13-2.jpg" alt="Product Name" class="product-hover-image">
                    </a>
                </figure>
                <div class="product-details-area">
                    <h2 class="product-name"><a href="demo-shop-2-product-details.html" title="Product Name">Diamond Ring - S</a></h2>
                    <div class="product-ratings">
                        <div class="ratings-box">
                            <div class="rating" style="width:0%"></div>
                        </div>
                    </div>

                    <div class="product-price-box">
                        <span class="product-price">$220.00</span>
                    </div>
                </div>
            </div>

            <div class="product product-sm">
                <figure class="product-image-area">
                    <a href="demo-shop-2-product-details.html" title="Product Name" class="product-image">
                        <img src="../img/demos/shop/products/product14.jpg" alt="Product Name">
                    </a>
                </figure>
                <div class="product-details-area">
                    <h2 class="product-name"><a href="demo-shop-2-product-details.html" title="Product Name">Diamond Ring - XL</a></h2>
                    <div class="product-ratings">
                        <div class="ratings-box">
                            <div class="rating" style="width:80%"></div>
                        </div>
                    </div>

                    <div class="product-price-box">
                        <span class="product-price">$180.00</span>
                    </div>
                </div>
            </div>

            <div class="product product-sm">
                <figure class="product-image-area">
                    <a href="demo-shop-2-product-details.html" title="Product Name" class="product-image">
                        <img src="../img/demos/shop/products/product15.jpg" alt="Product Name">
                    </a>
                </figure>
                <div class="product-details-area">
                    <h2 class="product-name"><a href="demo-shop-2-product-details.html" title="Product Name">Diamond Ring - 2XL</a></h2>
                    <div class="product-ratings">
                        <div class="ratings-box">
                            <div class="rating" style="width:0%"></div>
                        </div>
                    </div>

                    <div class="product-price-box">
                        <span class="product-price">$240.00</span>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <div class="product product-sm">
                <figure class="product-image-area">
                    <a href="demo-shop-2-product-details.html" title="Product Name" class="product-image">
                        <img src="../img/demos/shop/products/product16.jpg" alt="Product Name">
                        <img src="../img/demos/shop/products/product16-2.jpg" alt="Product Name" class="product-hover-image">
                    </a>
                </figure>
                <div class="product-details-area">
                    <h2 class="product-name"><a href="demo-shop-2-product-details.html" title="Product Name">Diamond Ring - 3XL</a></h2>
                    <div class="product-ratings">
                        <div class="ratings-box">
                            <div class="rating" style="width:60%"></div>
                        </div>
                    </div>

                    <div class="product-price-box">
                        <span class="product-price">$220.00</span>
                    </div>
                </div>
            </div>

            <div class="product product-sm">
                <figure class="product-image-area">
                    <a href="demo-shop-2-product-details.html" title="Product Name" class="product-image">
                        <img src="../img/demos/shop/products/product17.jpg" alt="Product Name">
                    </a>
                </figure>
                <div class="product-details-area">
                    <h2 class="product-name"><a href="demo-shop-2-product-details.html" title="Product Name">Diamond Ring - M</a></h2>
                    <div class="product-ratings">
                        <div class="ratings-box">
                            <div class="rating" style="width:0%"></div>
                        </div>
                    </div>

                    <div class="product-price-box">
                        <span class="product-price">$180.00</span>
                    </div>
                </div>
            </div>

            <div class="product product-sm">
                <figure class="product-image-area">
                    <a href="demo-shop-2-product-details.html" title="Product Name" class="product-image">
                        <img src="../img/demos/shop/products/product13.jpg" alt="Product Name">
                    </a>
                </figure>
                <div class="product-details-area">
                    <h2 class="product-name"><a href="demo-shop-2-product-details.html" title="Product Name">Diamond Ring - XL</a></h2>
                    <div class="product-ratings">
                        <div class="ratings-box">
                            <div class="rating" style="width:80%"></div>
                        </div>
                    </div>

                    <div class="product-price-box">
                        <span class="product-price">$240.00</span>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
</aside>