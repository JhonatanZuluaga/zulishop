<header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': false, 'stickyStartAt': 106, 'stickySetTop': '0', 'stickyChangeLogo': false}" class="header-full-width">
    <div class="header-body">
        <div class="header-container container">
            <div class="header-row">
                <div class="header-column">
                    <div class="header-logo">
                        <a href="<?php print(URL); ?>">
                            <img alt="Porto" width="111" height="50" src="<?php print(URL); ?>public/assets/images/logo.png">
                        </a>
                    </div>
                </div>
                <div class="header-column">
                    <div class="row">
                        <div class="header-nav-main">
                            <nav>
                                <ul class="nav nav-pills" id="mainNav">
                                    <li>
                                        <a href="<?php print(URL); ?>">
                                            Inicio
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php print(URL); ?>Product" >
                                            Productos <span class="tip tip-new">New</span>
                                        </a>

                                    </li>
                                    <li>
                                        <a href="<?php print(URL); ?>ShoppingCar">
                                            Carrito
                                        </a>
                                        </li>
                                    
                                </ul>
                            </nav>
                        </div>

                        <div class="dropdowns-container">
                           

                         

                            <div class="cart-dropdown">
                                <a href="#" class="cart-dropdown-icon">
                                    <i class="minicart-icon"></i>
                                    <span class="cart-info">
                                        <span class="cart-qty"><?php print sizeof($this->mycar); ?></span>
                                        <span class="cart-text">item(s)</span>
                                    </span>
                                </a>

                                <div class="cart-dropdownmenu right">
                                    <div class="dropdownmenu-wrapper">
                                        <div class="cart-products">

                                            <?php foreach ($this->mycar as $imagen) : ?>
                                                <?php foreach ($this->imagenesCool as $imagenes) : ?>
                                                    <?php if ($imagen->getId() == $imagenes->getId()) : ?>

                                                        <div class="product product-sm">
                                                            <a href="#" class="btn-remove eliminare" title="Remove Product" id="<?php print $imagen->getId(); ?>" >
                                                                <i class="fa fa-times"></i>
                                                            </a>
                                                            <figure class="product-image-area">
                                                                <a href="demo-shop-2-product-details.html" title="Product Name" class="product-image">
                                                                    <img src="<?php print(URL); ?>public/dist/img/products/<?php print $imagenes->getSrc(); ?>" alt="Product Name">
                                                                </a>
                                                            </figure>
                                                            <div class="product-details-area">
                                                                <h2 class="product-name"><a href="demo-shop-2-product-details.html" title="Product Name"><?php print $imagen->getName(); ?></a></h2>

                                                                <div class="cart-qty-price">
                                                                    1 X 
                                                                    <span class="product-price"><?php print "$" . $imagen->getPrice(); ?></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php endforeach; ?>

                                        </div>

                                        <div class="cart-totals">
                                            Total: <span><?php print "$" . $this->total; ?></span>
                                        </div>

                                        <div class="cart-actions">
                                            <a class="btn btn-primary" href="<?php print(URL);?>ShoppingCar">Ver Carrito</a>
                                              <a href="<?php print(URL);?>Product" class="btn btn-primary">Productos</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>	

                        <a href="#" class="mmenu-toggle-btn" title="Toggle menu">
                            <i class="fa fa-bars"></i>
                        </a>

                        <div class="header-search">
                            <a href="#" class="search-toggle"><i class="fa fa-search"></i></a>
                            <form action="#" method="get">
                                <div class="header-search-wrapper">
                                    <input type="text" class="form-control" name="q" id="q" placeholder="Search..." required>
                                    <select id="cat" name="cat">
                                        <option value="">All Categories</option>
                                        <option value="4">Fashion</option>
                                        <option value="12">- Women</option>
                                        <option value="13">- Men</option>
                                        <option value="66">- Jewellery</option>
                                        <option value="67">- Kids Fashion</option>
                                        <option value="5">Electronics</option>
                                        <option value="21">- Smart TVs</option>
                                        <option value="22">- Cameras</option>
                                        <option value="63">- Games</option>
                                        <option value="7">Home &amp; Garden</option>
                                        <option value="11">Motors</option>
                                        <option value="31">- Cars and Trucks</option>
                                        <option value="32">- Motorcycles &amp; Powersports</option>
                                        <option value="33">- Parts &amp; Accessories</option>
                                        <option value="34">- Boats</option>
                                        <option value="57">- Auto Tools &amp; Supplies</option>
                                    </select>
                                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                </div>
                            </form>
                        </div>

                        <div class="top-menu-area" id="userMenu">
                            <a href="#">Links <i class="fa fa-caret-down"></i></a>
                            <ul class="top-menu">
                                <?php if (Session::get("username")): ?>
                                    <li><a href="<?php print(URL); ?>myAccount"><?php print Session::get("username"); ?></a></li>
                                    <li data-option="logout"><a>Log out</a></li>
                                <?php else: ?>
                                    <li disabled><a href="<?php print(URL); ?>myAccount">My Account</a></li>
                                    <li><a href="<?php print(URL); ?>login">Log in</a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<script>

    $("#userMenu li").click(function () {
        var opt = $(this).data("option");

        switch (opt) {
            case "logout":
                $.ajax({
                    url: "<?php print(URL); ?>Clients/cerrarSesion/",
                    method: "GET"
                }).done(function () {
                    location.reload();
                });
                break;
        }
    });
    
    
    
    
//iniciamos la asignación de listeners

    addToCarrito();
//Función nueva! :D

    function addToCarrito() {
        //capturar los elementos
        var elementos = document.querySelectorAll(".eliminare");
        console.log(elementos);
        for (var i = 0; i < elementos.length; i++) {
            eventodePresion(elementos[i]);
            console.log(elementos);
        }
    }

    function eventodePresion(elemento) {
        console.log("hooli");
        elemento.onclick = function (e) {
            e.preventDefault();
            var data = {};
            var id = this.id;
            data = {
                Product_id: id
            };
            console.log(data);

            swal({
                title: "Esta seguro?",
                text: "Quiere eliminar el producto del Carrito?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, eliminar",
                cancelButtonText: "No, cancelar",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: "<?php print(URL); ?>ShoppingCar/eliminar",
                                method: "POST",
                                data: data
                            }).done(function (r) {
                                console.log(r);
                                //var r = JSON.parse(r);
                                if (r.error) {
                                    alert(r.msg);
                                } else {
                                    
                                     setTimeout(function () {
                                   swal("Eliminado!", "El producto se elimino del carrito", "success");
                                }, 2000, window.location.reload());
                                }
                            });

                        } else {
                            swal("Cancelado", "El producto no se elimino", "error");
                        }
                    });


//      







        }
    }

</script>