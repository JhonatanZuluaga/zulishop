<ul class="products-grid columns4">
    <?php foreach ($this->producticos as $product) : ?>
        <li>
            <div class="product">
                <figure class="product-image-area">
                    <a href="demo-shop-2-product-details.html" title="Product Name" class="product-image">      

                        <?php foreach ($this->imagencitas as $imagen) : ?>
                            <?php if ($product->getId() == $imagen->getId()) : ?>  
                                <img src="<?php print(URL); ?>public/dist/img/products/<?php print $imagen->getSrc(); ?>" alt="Product Name">
                                <img src="<?php print(URL); ?>public/dist/img/products/<?php print $imagen->getSrc(); ?>" alt="Product Name" class="product-hover-image">
                            <?php else : ?> 
                            <?php endif; ?> 
                        <?php endforeach; ?>
                    </a>

                    <a href="#" class="product-quickview">
                        <i class="fa fa-share-square-o"></i>
                        <span>Quick View</span>
                    </a>
                    <div class="product-label" style=<?php
                    $x = ($product->getDisscount() == 0) ? 'display:none' : 'display:block';
                    print $x
                    ?>><span class="discount"><?php print $product->getDisscount() ?>%</span></div>
                    <div class="product-label"><span class="new">New</span></div>
                </figure>
                <div class="product-details-area">
                    <h2 class="product-name"><a href="demo-shop-2-product-details.html" title="Product Name"><?php print $product->getName(); ?></a></h2>
                    <div class="product-ratings">
                        <div class="ratings-box">
                            <div class="rating" style="width:60%"></div>
                        </div>
                    </div>

                    <div class="product-price-box">
                        <span class="old-price" style=<?php
                              $y = ($product->getPrice() == ($z = $product->getPrice() - ($product->getPrice() * ($product->getDisscount() / 100)))) ? 'display:none' : 'display:block';
                              print $y
                              ?>><?php print $z; ?></span>
                        <span class="product-price"><?php print $product->getPrice(); ?></span>
                    </div>

                    <div class="product-actions sisas" >
                        <a href="#" class="addtowishlist" title="Add to Wishlist">
                            <i class="fa fa-heart"></i>
                        </a>
                        <a href="#" class="addtocart preiona" title="Add to Cart" id="<?php print $product->getId(); ?>" value="<?php print $product->getId(); ?>">
                            <i class="fa fa-shopping-cart "></i>
                            <span>Agregar</span>
                        </a>
                        <a href="#" class="comparelink" title="Add to Compare">
                            <i class="glyphicon glyphicon-signal"></i>
                        </a>
                    </div>
                </div>
            </div>
        </li>
<?php endforeach; ?>
</ul>

<script type="text/javascript">


//iniciamos la asignación de listeners

    addToCarrito();
//Función nueva! :D

    function addToCarrito() {
        //capturar los elementos
        var elementos = document.querySelectorAll(".preiona");
        console.log(elementos);
        for (var i = 0; i < elementos.length; i++) {
            eventodePresion(elementos[i]);
            console.log(elementos);
        }
    }

    function eventodePresion(elemento) {
        console.log("hooli");
        elemento.onclick = function (e) {
            e.preventDefault();
            data={};
            var id = this.id;
            data={
                Product_id: id
            };
            swal({
                title: "Quieres ir al carrito de compras?",
                text: "Pudes quedarte agregando mas productos",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, Ir!",
                cancelButtonText: "No, seguir comprando!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                    function (isConfirm) {
                        if (isConfirm) {
                              $.ajax({
                                url: "<?php print(URL); ?>ShoppingCar/agregar",
                                method: "POST",
                                data: data
                            }).done(function (r) {
                                console.log(r);
                                //var r = JSON.parse(r);
//                                if (r.error) {
//                                    alert(r.msg);
//                                } else {
                                   window.location.replace("<?php print(URL); ?>ShoppingCar/Index");
//                                }
                            });
                           
                        } else {
                            $.ajax({
                                url: "<?php print(URL); ?>ShoppingCar/agregar",
                                method: "POST",
                                data: data
                            }).done(function (r) {
                                console.log(r);
                                //var r = JSON.parse(r);
//                                if (r.error) {
//                                    alert(r.msg);
//                                } else {
                                    swal("Agregado", "Tu compra fue agregada", "success");
//                                }
                            });


                        }
                    });



        }
    }

//

    function processForm(form) {
        var inputs = form.querySelectorAll("input");
        var data = {};
        $(inputs).each(function () {
            var input = $(this);
            if (input.context.type != "submit") {
                var attr = input.context.name;
                data[attr] = input.context.value;

            }
        });
        return data;
    }



    function addElementListenners(elemento) {

        elemento.onclick = function () {
            var id = this.id;
            if (id != null) {
                var data = {
                    id: id
                };
                $.ajax({
                    url: "<?php print(URL); ?>Products/eliminarProducto",
                    method: "POST",
                    data: data
                }).done(function (r) {
                    console.log(r);
                    var r = JSON.parse(r);
                    if (r.error) {
                        alert(r.msg);
                    } else {
                        alert(r.msg);
<?php $this->reloadThis("Products/index"); ?>;
                    }
                });
            }
        }
    }

</script>
