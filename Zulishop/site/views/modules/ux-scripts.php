
<script src="<?php print(URL); ?>public/frameworks/jquery.appear/jquery.appear.min.js"></script>
<script src="<?php print(URL); ?>public/frameworks/jquery.easing/jquery.easing.min.js"></script>
<script src="<?php print(URL); ?>public/frameworks/jquery-cookie/jquery-cookie.min.js"></script>
<script src="<?php print(URL); ?>public/frameworks/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php print(URL); ?>public/frameworks/common/common.min.js"></script>
<script src="<?php print(URL); ?>public/frameworks/jquery.validation/jquery.validation.min.js"></script>
<script src="<?php print(URL); ?>public/frameworks/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
<script src="<?php print(URL); ?>public/frameworks/jquery.gmap/jquery.gmap.min.js"></script>
<script src="<?php print(URL); ?>public/frameworks/jquery.lazyload/jquery.lazyload.min.js"></script>
<script src="<?php print(URL); ?>public/frameworks/isotope/jquery.isotope.min.js"></script>
<script src="<?php print(URL); ?>public/frameworks/owl.carousel/owl.carousel.min.js"></script>
<script src="<?php print(URL); ?>public/frameworks/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="<?php print(URL); ?>public/frameworks/vide/vide.min.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="<?php print(URL); ?>public/js/theme.js"></script>


<script src="<?php print(URL); ?>public/frameworks/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="<?php print(URL); ?>public/frameworks/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<script src="<?php print(URL); ?>public/frameworks/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
<script src="<?php print(URL); ?>public/frameworks/elevatezoom/jquery.elevatezoom.js"></script>
<script src="<?php print(URL); ?>public/frameworks/sweetalert/sweetalert.min.js"></script>

<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-581b726c069c6315"></script>


<!-- Demo -->
<script src="<?php print(URL); ?>public/js/demo-shop-2.js"></script>

<!-- Theme Custom -->
<script src="<?php print(URL); ?>public/js/custom.js"></script>

<!-- Theme Initialization Files -->
<script src="<?php print(URL); ?>public/js/theme.init.js"></script>