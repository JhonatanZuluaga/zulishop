<section class="form-section register-form">
    <div class="container">
        <h1 class="h2 heading-primary font-weight-normal mb-md mt-lg">Mi Perfil</h1>

        <div class="featured-box featured-box-primary featured-box-flat featured-box-text-left mt-md">
            <div class="box-content">
                <form id="formularioCool">

                    <h4 class="heading-primary text-uppercase mb-lg">EDITAR INFORMACION PERSONAL</h4>
                    <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="font-weight-normal">Email <span class="required">*</span></label>
                            <input type="email" class="form-control" required name="email">
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="font-weight-normal">Contraseña Actual <span class="required">*</span></label>
                            <input type="password" class="form-control" required name="passwordCon">
                        </div>
                    </div>
                    </div>

                    <div class="checkbox mb-xs">
                        <label>
                            <input type="checkbox" value="1" id="change-pass-checkbox">
                            Cambiar tu Contraseña
                        </label>
                    </div>

                    <div id="account-chage-pass">
                        <h4 class="heading-primary text-uppercase mb-lg">CAMBIAR CONTRASEÑA</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="font-weight-normal">Nueva Contraseña <span class="required">*</span></label>
                                    <input type="password" class="form-control"  name="password">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="font-weight-normal">Confirmar Contraseña <span class="required">*</span></label>
                                    <input type="password" class="form-control"  name="passwordR">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <p class="required mt-lg mb-none">* Requiere datos</p>

                            <div class="form-action clearfix mt-none">
                                <input type="submit" class="btn btn-primary" id="nicolas"value="Guardar">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript"> 
    
    
    
    var readyToSend = false;
    var errorMsg = "";
	$("#formularioCool input ").on("blur keyup keydown",function(e){
		
			
				var form = document.querySelector("#formularioCool");

				if(e.type == "blur"){
					var email = form.querySelector("[name=email]");
					validateUnique(email,"Clients","verificarCorreo","email");
				}

                                if($('#account-chage-pass').is('.show')){
                                    console.log("wiiii");
				var pass = form.querySelector("[name=password]");
				var rPass = form.querySelector("[name=passwordR]");
//                                console.log(pass.value+"   "+rPass.value);

				if( pass.value != '' && rPass.value != ''){
					if(pass.value == rPass.value){
						$(pass).removeClass("incorrecto").addClass("correcto");
						$(rPass).removeClass("incorrecto").addClass("correcto");

						readyToSend = true;

					}else{
						$(pass).removeClass("correcto").addClass("incorrecto");
						$(rPass).removeClass("correcto").addClass("incorrecto");

						readyToSend = false;
						errorMsg = "Los passwords no coinciden";
					}
				}
			}else{
                            readyToSend = true;
                        }
		
	})
        
        function validateUnique(who, controller, method, name) {
   
        if (who.value != '') {
            $.ajax({
                url: "<?php print(URL); ?>" + controller + "/" + method + "/" + who.value,
                method: "GET"
            }).done(function (r) {
                if (r != 0) {
                    $(who).removeClass("correcto").addClass("incorrecto");
                    readyToSend = false;
                    errorMsg = name + " existente en la base de datos";
                } else {
                    $(who).removeClass("incorrecto").addClass("correcto");
                    readyToSend = true;
                }
            });
        }

    }

 
    
    
    	$("#nicolas").click(function(e){
		e.preventDefault();

		var form = document.querySelector("#formularioCool");
		var data = {}

		//Trigger ejecutando un blur en cada input
		var inputs = form.querySelectorAll("input");
		$(inputs).each(function(){
			if($(this).hasClass("incorrecto")){
				readyToSend = false;
			}
		});
                
		
		if(readyToSend){
                                if($('#account-chage-pass').is('.show')){
				data = processForm(form);
                                console.log(data);
                                    }else{
                                    var emai = form.querySelector("[name=email]").value;
                                    var pass = form.querySelector("[name=passwordCon]").value;
                                    data = {
                                        emai: emai,
                                        passwordCon: pass
                                    }
                                    console.log(data);
                                }
					$.ajax({
						url:"<?php print(URL);?>Clients/editarCliente/",
						method:"POST",
						data: data
					}).done(function(r){
						if(JSON.parse(r)){
							var r = JSON.parse(r);
							if(r.error == 0){
								alert(r.msg);
								location.reload();
							}else{
								alert(r.msg);
							}
						}
						
					});
//				
			

		}else{
			alert(errorMsg);
		}
		
		//return false;
	});

       function processForm(form) {

        var inputs = form.querySelectorAll("input");
        
        var data = {};
       $(inputs).each(function () {
            var input = $(this);
            
          if (input[0].type != "submit" && input[0].type != "checkbox") {
                var attr = input[0].name;
                data[attr] = input[0].value;
                    
          }
       });

        return data;
    }
    

</script>