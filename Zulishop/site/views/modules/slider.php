<div class="slider-container rev_slider_wrapper" style="height: 100vh;">
    <div id="revolutionSlider" class="slider rev_slider manual">
        <ul>
            <li data-transition="fade">
                <img src="<?php print(URL); ?>public/dist/img/slides/slidePrueba2.jpg"  
                    alt="slide bg"
                    data-bgposition="center center" 
                    data-bgfit="cover" 
                    data-bgrepeat="no-repeat"
                    class="rev-slidebg">

                <div class="tp-caption"
                    data-x="15"
                    data-y="center" data-voffset="-85"
                    data-start="500"
                    data-whitespace="nowrap"						 
                    data-transform_in="y:[100%];s:500;"
                    data-transform_out="opacity:0;s:500;"
                    data-responsive_offset="on"
                    style="z-index: 5; font-size: 26px; text-transform: uppercase; font-weight:600; line-height:1; color: #fff;"
                    data-mask_in="x:0px;y:0px;">Nuevos Productos</div>

                <div class="tp-caption"
                    data-x="12"
                    data-y="center" data-voffset="-31"
                    data-start="1000"
                    data-whitespace="nowrap"						 
                    data-transform_in="y:[100%];s:500;"
                    data-transform_out="opacity:0;s:500;"
                    data-responsive_offset="on"
                    style="z-index: 5; font-size: 80px; font-weight:800; line-height:1.4; color: #fff; letter-spacing: -6px; padding-right:10px;"
                    data-mask_in="x:0px;y:0px;">Tecnologia</div>

                <div class="tp-caption"
                    data-x="15"
                    data-y="center" data-voffset="28"
                    data-start="1500"
                    data-responsive_offset="on"
                    style="z-index: 5; font-size: 25px; font-weight: 300; line-height:1; color: #fff;"
                    data-transform_in="y:[100%];opacity:0;s:500;">Descuentos del <span style="font-weight:800;">70% OFF</span> en nueva tecnologia.</div>
                
                <div class="tp-caption tp-resizeme"
                    data-x="15"
                    data-y="center" data-voffset="85"
                    data-start="2000"
                    data-responsive_offset="on"
                    style="z-index: 5; font-size: 16px; font-weight: 600; line-height:1;"
                    data-transform_in="y:[100%];opacity:0;s:500;"><a href="<?php print(URL); ?>Product" class="btn btn-default">Compra ahora</a></div>
            </li>
            <li data-transition="fade">
                <img src="<?php print(URL); ?>public/dist/img/slides/slidePrueba.jpg"  
                    alt="slide bg"
                    data-bgposition="center center" 
                    data-bgfit="cover" 
                    data-bgrepeat="no-repeat"
                    class="rev-slidebg">

                <div class="tp-caption"
                    data-x="15"
                    data-y="center" data-voffset="-85"
                    data-start="500"
                    data-whitespace="nowrap"						 
                    data-transform_in="y:[100%];s:500;"
                    data-transform_out="opacity:0;s:500;"
                    data-responsive_offset="on"
                    style="z-index: 5; font-size: 26px; text-transform: uppercase; font-weight:600; line-height:1; color: #fff;padding-right:10px"
                    data-mask_in="x:0px;y:0px;">Exclusivos</div>

                <div class="tp-caption"
                    data-x="14"
                    data-y="center" data-voffset="-31"
                    data-start="1000"
                    data-whitespace="nowrap"						 
                    data-transform_in="y:[100%];s:500;"
                    data-transform_out="opacity:0;s:500;"
                    data-responsive_offset="on"
                    style="z-index: 5; font-size: 80px; font-weight:800; line-height:1.4; color: #fff; letter-spacing: -6px; padding-right:10px;"
                    data-mask_in="x:0px;y:0px;">Tendencias</div>

                <div class="tp-caption"
                    data-x="225"
                    data-y="center" data-voffset="28"
                    data-start="1500"
                    data-responsive_offset="on"
                    style="z-index: 5; font-size: 25px; font-weight: 300; line-height:1; color: #fff;"
                    data-transform_in="y:[100%];opacity:0;s:500;">Increible Precios</div>

                <div class="tp-caption"
                    data-x="320" 
                    data-y="center" data-voffset="70"
                    data-start="2000"
                    data-responsive_offset="on"
                    style="z-index: 5; font-size: 16px; font-weight: 400; line-height:1; color:#fff;"
                    data-transform_in="y:[100%];opacity:0;s:500;"><a href="<?php print(URL); ?>Product" class="btn btn-default" style="text-transform: uppercase;" >Compra ahora</a></div>
            </li>
        </ul>
    </div>
</div>