<div class="container mb-sm">
    <h2 class="slider-title v2 heading-primary text-center">
        DISCOUNT PRODUCTS
    </h2>
    
    <div class="owl-carousel owl-theme manual featured-products-carousel">
        
        <?php $contadorA=0; $contadorB=0;?>
        <?php $array = array(1, 2, 3, 4); ?>
        <?php $arrayB = array(1, 2, 3, 4); ?>
        <?php $organizado= FALSE ?>
        <?php foreach ($this->producticos as $product) : ?>      
        <?php  $descuentos= $product->getDisscount();
        
               if($descuentos > 0 && $contadorA!=4 ):

                $array[$contadorA]=$descuentos; 
                $contadorA= $contadorA+1;

               endif;
               
               if($contadorA==4 && $organizado==FALSE):
                   //[10,10,2,5]
                   for ($i = 0; $i < 4; $i++) {
                    
                       for ($j = $i+1 ; $j < 4; $j++) {
                       if($array[$i]<$array[$j]):
                          $contadorB=$array[$i];   
                          $array[$j]=$array[$i];
                          $array[$j]=$contadorB;
                        endif;
                        
                       }
                    
                   }                    
                   $organizado=TRUE;
                   
               endif;
               
               if($contadorA==4 && $organizado==TRUE):
                    
                   if($descuentos>=$arrayB[0]):
                       $arrayB[0]=$descuentos;
                   elseif($descuentos>=$arrayB[1]):
                       $arrayB[1]=$descuentos;
                   elseif($descuentos>=$arrayB[2]):
                       $arrayB[2]=$descuentos;
                   elseif($descuentos>=$arrayB[3]):
                       $arrayB[3]=$descuentos;
                   elseif($descuentos>=$arrayB[4]):
                       $arrayB[4]=$descuentos;         
                   endif;
                   
                   
               endif;
        ?>
        <?php endforeach; ?>
        <!--Array ( [0] => 50 [1] => 39 [2] => 35 [3] => 22 [4] => 20 )-->
        <?php 
           $estado1=TRUE;
           $estado2=TRUE;
           $estado3=TRUE;
           $estado4=TRUE;
           
          ?>
        <?php foreach ($this->producticos as $product) : ?>
            
         <?php if((($product->getDisscount() == $arrayB[0])&&($estado1))
                 ||(($product->getDisscount() == $arrayB[1])&&($estado2))
                 ||(($product->getDisscount() == $arrayB[3])&&($estado3))
                 ||(($product->getDisscount() == $arrayB[4])&&($estado4)))  :  ?>  
          <?php 
          
        
           if($product->getDisscount()==$arrayB[0]):    
               $estado1= FALSE;
           endif;
           
           if($product->getDisscount()==arrayB[1]):
               $estado2= FALSE;
           endif;
           
           if($product->getDisscount()==arrayB[2]):
               $estado3= FALSE;
           endif;
           
           if($product->getDisscount()==arrayB[3]):
               $estado4= FALSE;
           endif;
           
          ?>  
          
        <div class="product">
            <figure class="product-image-area">
                
                <a href="demo-shop-2-product-details.html" title="Product Name" class="product-image">
                    <?php foreach ($this->imagencitas as $imagen) : ?>
                    <?php if($product->getId()==$imagen->getId()) :  ?>  
                    <img src="<?php print(URL); ?>public/dist/img/products/<?php print $imagen->getSrc(); ?>" alt="Product Name">
                    <?php else :?> 
                    <?php endif ;?> 
                    <?php endforeach; ?>
                </a>

                <a href="#" class="product-quickview">
                    <i class="fa fa-share-square-o"></i>
                    <span>Quick View</span>
                </a>
                <div class="product-label" style=<?php $x = ($product->getDisscount()==0) ? 'display:none' : 'display:block'; print $x ?>><span class="discount"><?php  print $product->getDisscount()   ?>%</span></div>
                <div class="product-label"><span class="new">New</span></div>
            </figure>
            <div class="product-details-area">
                <h2 class="product-name"><a href="demo-shop-2-product-details.html" title="Product Name"><?php print $product->getName(); ?></a></h2>
                <div class="product-ratings">
                    <div class="ratings-box">
                        <div class="rating" style="width:60%"></div>
                    </div>
                </div>

                <div class="product-price-box">
                    <span class="old-price" style=<?php $y = ($product->getPrice()==($z=$product->getPrice()-($product->getPrice()*($product->getDisscount()/100)))) ? 'display:none' : 'display:block'; print $y ?>><?php print $z; ?></span>
                    <span class="product-price"><?php print $product->getPrice(); ?></span>
                </div>
            </div>
        </div>
        <?php endif ;?> 
        <?php endforeach; ?>
       
    </div>
</div>