<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Clients_controller extends BController {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        //$this->view->render($this, "index");
    }

    public function entrar() {
        $res = [];
        if (filter_input(INPUT_POST, "email") != null && filter_input(INPUT_POST, "password") != null) {
            $usr = Client::getBy("email", filter_input(INPUT_POST, "email"));
            if (!is_null($usr)) {
                $r = json_encode(Clients_bl::login($usr, filter_input(INPUT_POST, "password")));


                if ($r == 0) {
                    $this->crearSesion($usr);
                    $res = ["error" => 0, "msg" => "Bienvenido Pimpollo"];
                } else {
                    $res = ["error" => 1, "msg" => "No se Pudo Iniciar socio"];
                }
            }
            echo json_encode($res);
        }
    }

    public function registrar() {
        if (isset($_POST)) {


            $obj = Clients_bl::registroClient($_POST);

            if ($obj != 0) {
                $this->crearSesion($obj);
                $r = ["error" => 0, "msg" => "Correctamente Creado"];
            } else {
                $r = ["error" => 1, "msg" => "No se Pudo registrar, hable con pablo para que suba la nota"];
            }
        } else {
            $r = ["error" => 1, "msg" => "Debe proveer todos los datos"];
        }

        echo json_encode($r);
    }

    public static function crearSesion(Client $usr) {

        Session::set("username", $usr->getUsername());
        Session::set("idUser", $usr->getId());
    }

    public static function cerrarSesion() {

        Session::remove("username");
        Session::remove("idUser");
    }

    public function verificarUsername($username, $ajax = true) {
        $r = Client::getBy("username", $username);
        if ($ajax) {
            $r = (empty($r)) ? 0 : 1;
            echo $r;
        } else {
            $r = (empty($r)) ? 0 : $r;
            return $r;
        }
    }

    public function verificarCorreo($email, $ajax = true) {
        $r = Client::getBy("email", $email);
        if ($ajax) {
            $r = (empty($r)) ? 0 : 1;
            echo $r;
        } else {
            $r = (empty($r)) ? 0 : $r;
            return $r;
        }
    }

    public function mostrar() {
        echo Session::get("username");
    }

    public function editarCliente() {
        $r = [];
        if (isset($_POST)) {
            $array = $_POST;
            if (sizeof($array) >= 3) {

                $usr = Client::getBy("username", Session::get("username"));
                $respon = Clients_bl::compararPassword($usr->getPassword(), $_POST["passwordCon"]);
               
               if ($respon == 1) {
                  $usr->setEmail($_POST["email"]);
    
                  $newPas = Clients_bl::cripMypass($_POST["password"]);
                  
                   $usr->setPassword($newPas);
                   $response = Clients_bl::actualizar($usr);
                    if ($response) {
                        $r = ["error" => 0, "msg" => "Actualizado correctamente"];
                    } else {
                       $r = ["error" => 1, "msg" => "No se pudo actualizar"];
                   }
                }
            } else {
                $usr = Client::getBy("username", Session::get("username"));
               $respon = Clients_bl::compararPassword($usr->getPassword(), $_POST["passwordCon"]);

               if ($respon == 1) {
                  $usr->setEmail($_POST["emai"]);
                   $response = Clients_bl::actualizar($usr);
                    if ($response) {
                        $r = ["error" => 0, "msg" => "Actualizado correctamente"];
                    } else {
                        $r = ["error" => 1, "msg" => "No se pudo actualizar"];
                    }
                }
            }
        } else {
            $r = ["error" => 1, "msg" => "Debe proveer todos los datos"];
        }
      echo json_encode($r);
    }

}
