<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Index_controller extends Controller{
   
    function __construct() {
        parent::__construct();
    }

    public function index()
    {
        //$this->view->debug = true;
        
        //$usrCtrlr = new Users_controller();
       // $this->view->usrCtrlr = $usrCtrlr;
        $this->view->mycar = ShoppingCar_bl::carritoPorCliente();
        $this->view->imagenesCool = ShoppingCar_bl::imagenesPorCarrito();
        $this->view->total =ShoppingCar_bl::total();
        $this->view->producticos = Products_bl::getAll2();
        $this->view->categoriticas = Categories_bl::getAll2();
        $this->view->imagencitas = Images_bl::getAll2();
        $this->view->render($this,"index","Zulishop - ecommerce");
    }
    
}
