<?php

/**
* Class ShoppingCar generated by Chrysalis v.1.0.0
* 
* Chrysalis info:
* @author pabhoz
* github: @pabhoz
* bitbucket: @pabhoz
* 
*/ 
 
class ShoppingCar extends BModel {

    private $id;
    private $Product_id;
    private $quantity;
    
    private $has_one = array(
      'productShoppingCar'=>array(
          //Hago la relacion del Product -> Brand 
          'class'=>'Product',
          'join_as'=>'id',
          'join_with'=>'id'
          ),//Hago la relacion del Product -> image 
           'clientShoppingCar'=>array(
          'class'=>'Client',
          'join_as'=>'id',
          'join_with'=>'id'
          )
      );

    public function __construct( $id,  $Product_id,  int $quantity) {

        parent::__construct();
        $this->id = $id;
        $this->Product_id = $Product_id;
        $this->quantity = $quantity;
    }

    public function getId(): int {
        return $this->id;
    }

    public function getProduct_id(): int {
        return $this->Product_id;
    }

    public function getQuantity(): int {
        return $this->quantity;
    }

    public function setId(int $id) {
        $this->id = $id;
    }

    public function setProduct_id(int $Product_id) {
        $this->Product_id = $Product_id;
    }

    public function setQuantity(int $quantity) {
        $this->quantity = $quantity;
    }

    public function getMyVars(){
        return get_object_vars($this);
    }
    function getHas_one() {
        return $this->has_one;
    }

    function setHas_one($has_one) {
        $this->has_one = $has_one;
    }



}