<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Providers_controller extends BController{

     function __construct() {
        parent::__construct();
    }
    public function index()
    {
        $this->view->provides = Providers_bl::getAll();
        $this->view->render($this,"index");
    }
    
       public function create() {
        $r = [];
        if (isset($_POST)) {
        

            $response = Providers_bl::create($_POST);
            
            if ($response) {
                $r = ["error" => 0, "msg" => "creado correctamente"];
            } else {
                $r = ["error" => 1, "msg" => "No se pudo crear"];
            }
        } else {
            $r = ["error" => 1, "msg" => "Debe proveer todos los datos"];
        }
     echo json_encode($r);
        
    }

    public function editarProvider() {
        $r = [];
        if (isset($_POST)) {
           $keys = Provider::getKeys();
           
           $this->validateKeys($keys, filter_input_array(INPUT_POST));
           $prov = Provider::instanciate($_POST);
          // print_r($prov);
           $response = Providers_bl::actualizar($prov);
            print_r($_POST);
            if (true) {
                $r = ["error" => 0, "msg" => "Actualizado correctamente"];
            } else {
                $r = ["error" => 1, "msg" => "No se pudo actualizar"];
            }
           
        } else {
            $r = ["error" => 1, "msg" => "Debe proveer todos los datos"];
        }
         //echo json_encode($prov);
    }

    
    public function eliminarProvider() {
        $r = [];

        if (isset($_POST["id"])) {
            $id = $_POST["id"];
            
           $prov = Provider::getById($id);          
            $response = Providers_bl::eliminar($prov);
            if ($response) {
                $r = ["error" => 0, "msg" => "Eliminado"];
               //$this->view->reloadThis($this);
            } else {
                $r = ["error" => 1, "msg" => "No se pudo eliminar"];
            }

        } else {
            $r = ["error" => 1, "msg" => "Debe proveer todos los datos"];
        }
        
       print(json_encode($r));
    }

}
