<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Clients_controller extends BController {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->mostrarTodos();
        $this->view->clients = Clients_bl::getAll();
        $this->view->render($this, "index");
    }
    
     public function mostrarTodos(){
//       $usr = Users_bl::getAll();
        $this->view->users = Clients_bl::getAll();
    }

    public function eliminarClient() {
        $r = [];

        if (isset($_POST["id"])) {
            $id = $_POST["id"];
            
           $cliente = Client::getById($id);          
            $response = Clients_bl::eliminar($cliente);
            if ($response) {
                $r = ["error" => 0, "msg" => "Eliminado"];
               //$this->view->reloadThis($this);
            } else {
                $r = ["error" => 1, "msg" => "No se pudo eliminar"];
            }

        } else {
            $r = ["error" => 1, "msg" => "Debe proveer todos los datos"];
        }
        
       print(json_encode($r));
    }

    public function editarClient() {
        $r = [];
        if (isset($_POST)) {
           $keys = Client::getKeys();
           
           $this->validateKeys($keys, filter_input_array(INPUT_POST));
           $cliente = Client::instanciate($_POST);
          // print_r($cliente);
           $response = Clients_bl::actualizar($cliente);
            print_r($_POST);
            if (true) {
                $r = ["error" => 0, "msg" => "Actualizado correctamente"];
            } else {
                $r = ["error" => 1, "msg" => "No se pudo actualizar"];
            }
           
        } else {
            $r = ["error" => 1, "msg" => "Debe proveer todos los datos"];
        }
         //echo json_encode($cliente);
    }

}
