<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Users_controller extends BController {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        //$this->view->users = Users_bl::getAll();
       $this->mostrarTodos();
       $this->mostrarRoles();
       //$this->traerUser();
       $this->view->render($this, "index");
//         $this->view->render($this, "edit");
     
        
        
    }

    public function eliminarUsuario() {
        $r = [];

        if (isset($_POST["id"])) {
            $id = $_POST["id"];
            
           $usr = User::getById($id);          
            $response = Users_bl::eliminar($usr);
            if ($response) {
                $r = ["error" => 0, "msg" => "Eliminado"];
               //$this->view->reloadThis($this);
            } else {
                $r = ["error" => 1, "msg" => "No se pudo eliminar"];
            }

        } else {
            $r = ["error" => 1, "msg" => "Debe proveer todos los datos"];
        }
        
       print(json_encode($r));
    }

    public function editarUsuario() {
        $r = [];
        if (isset($_POST)) {
           $keys = User::getKeys();
           
           $this->validateKeys($keys, filter_input_array(INPUT_POST));
           $usr = User::instanciate($_POST);
          // print_r($usr);
           $response = Users_bl::actualizar($usr);
            print_r($_POST);
            if (true) {
                $r = ["error" => 0, "msg" => "Actualizado correctamente"];
            } else {
                $r = ["error" => 1, "msg" => "No se pudo actualizar"];
            }
           
        } else {
            $r = ["error" => 1, "msg" => "Debe proveer todos los datos"];
        }
         //echo json_encode($usr);
    }
    
    public function mostrarTodos(){
//       $usr = Users_bl::getAll();
        $this->view->users = Users_bl::getAll();
    }
    //funcion para traer el usuario que quiere editar
    
    public function mostrarRoles(){
        $this->view->roles = Rols_bl::getAll();
    }
    
    public function create(){
         
        $r = [];
        if (isset($_POST)) {
        
            print_r($_POST);
           $response = Users_bl::create($_POST);
            
            if ($response) {
                $r = ["error" => 0, "msg" => "creado correctamente"];

            } else {
                $r = ["error" => 1, "msg" => "No se pudo crear"];
            }
        } else {
            $r = ["error" => 1, "msg" => "Debe proveer todos los datos"];
        }
    echo json_encode($r);
    }

//    public function tracking(){
//        $r = [];
//        if(isset($_POST)){
//
//            $session = Session::get("username")
//            $usr = Users_bl::getByUsername($session)->getId();
//            $action = Action::getBy("action", $_POST["action"]);
//            $entity = $_POST["entity"];
//            $idE = $_POST["id"];
//
//
//            $usr->has_many("actions",$action);
//            $usr->update();
//
//        }
//    }
    

}
