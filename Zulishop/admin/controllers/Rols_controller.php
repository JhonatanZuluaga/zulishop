<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Rols_controller
 *
 * @author pabhoz
 */
class Rols_controller extends BController{

    function __construct() {
        parent::__construct();
    }

    public function index()
    {
        // $this->traerItemMenu();
        $this->view->roles = Rols_bl::getAll();
        $this->view->render($this,"index");
    }

    public function create(){
         
        $r = [];
        if (isset($_POST)) {
        

            $response = Rols_bl::create($_POST);
            
            if ($response) {
                $r = ["error" => 0, "msg" => "creado correctamente"];

            } else {
                $r = ["error" => 1, "msg" => "No se pudo crear"];
            }
        } else {
            $r = ["error" => 1, "msg" => "Debe proveer todos los datos"];
        }
     echo json_encode($r);
    }
//    public function init(){
//        Rols_bl::traking();
//    }

    public function eliminarRol() {
        $r = [];

        if (isset($_POST["id"])) {
            $id = $_POST["id"];
            
        $rol = Rol::getById($id);          
            $response = Rols_bl::eliminar($rol);
            if ($response) {
                $r = ["error" => 0, "msg" => "Eliminado"];
            //$this->view->reloadThis($this);
            } else {
                $r = ["error" => 1, "msg" => "No se pudo eliminar"];
            }

        } else {
            $r = ["error" => 1, "msg" => "Debe proveer todos los datos"];
        }
        
    print(json_encode($r));
    }

    public function editarRol() {
        $r = [];
        if (isset($_POST)) {
        $keys = Rol::getKeys();
        
        $this->validateKeys($keys, filter_input_array(INPUT_POST));
        $rol = Rol::instanciate($_POST);
        // print_r($rol);
        $response = Rols_bl::actualizar($rol);
            print_r($_POST);
            if (true) {
                $r = ["error" => 0, "msg" => "Actualizado correctamente"];
            } else {
                $r = ["error" => 1, "msg" => "No se pudo actualizar"];
            }
        
        } else {
            $r = ["error" => 1, "msg" => "Debe proveer todos los datos"];
        }
        //echo json_encode($cliente);
    }

    // public function traerItemMenu(){
    //     $this->view->menus = Menus_bl::getBy("rol",$_POST); // trae el menu por el rol
    // }

}
