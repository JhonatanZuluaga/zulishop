<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Images_controller extends BController {

    function __construct() {
        parent::__construct();
    }

    public function index() {
     $this->view->imagens = Images_bl::getAll2();
     $this->view->prducticos = Images_bl::getMyProduct();
        $this->view->render($this, "index");
    }

    public function create() {
        $r = [];
        if (isset($_POST)) {
            //print_r($_POST);
            $archivo = $_FILES['src']; //mostrando que me llega la imagen en el formato correcto
            $r = Images_bl::subirFoto($archivo); //muevo la imagen a mi serve
            $i = substr($r, 16); //reduzco el nombre de la img 
            $dato = array("id" => $_POST["id"], "src" => $i); //creo el arreglo para la instanciacion

            $response = Images_bl::create($dato);

            if ($response) {
                $r = ["error" => 0, "msg" => "creado correctamente"];

            } else {
                $r = ["error" => 1, "msg" => "No se pudo crear"];
            }
        } else {
            $r = ["error" => 1, "msg" => "Debe proveer todos los datos"];
        }


        echo json_encode($r);
    }

}
