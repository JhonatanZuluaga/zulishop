<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Index_controller extends BController{

    function __construct() {
        parent::__construct();
    }

    public function index()
    {
        $uid = Session::get("aid");
        if(empty($uid)){ header("Location:".URL."Login");}
        $this->view->user = Users_bl::getUser($uid); //print_r($this->view->user); trae usuario de la session
        $this->view->menus = Menus_bl::getUserMenus($this->view->user->getRol()); // trae el menu por el rol
        $this->view->title="ZuliShop Panel Administrativo";
        $this->view->render($this,"index");
    }

}
