<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Brands_controller extends BController {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->view->brands = Brands_bl::getAll();
        $this->view->render($this, "index");
    }

    public function create() {
        $r = [];
        if (isset($_POST)) {
            //print_r($_POST);
            $archivo = $_FILES['logo']; //mostrando que me llega la imagen en el formato correcto
            $r = Brands_bl::subirFoto($archivo); //muevo la imagen a mi serve
            $i = substr($r, 16); //reduzco el nombre de la img 
            $dato = array("name" => $_POST["name"], "logo" => $i); //creo el arreglo para la instanciacion

            $response = Brands_bl::create($dato);

            if ($response) {
                $r = ["error" => 0, "msg" => "creado correctamente"];

            } else {
                $r = ["error" => 1, "msg" => "No se pudo crear"];
            }
        } else {
            $r = ["error" => 1, "msg" => "Debe proveer todos los datos"];
        }


        echo json_encode($r);
    }

    public function eliminarBrand() {
        $r = [];

        if (isset($_POST["id"])) {
            $id = $_POST["id"];
            
           $brand = Brand::getById($id);          
            $response = Brands_bl::eliminar($brand);
            if ($response) {
                $r = ["error" => 0, "msg" => "Eliminado"];
               //$this->view->reloadThis($this);
            } else {
                $r = ["error" => 1, "msg" => "No se pudo eliminar"];
            }

        } else {
            $r = ["error" => 1, "msg" => "Debe proveer todos los datos"];
        }
        
       print(json_encode($r));
    }

    public function editarBrand() {
        $r = [];
        if (isset($_POST)) {
           $keys = Brand::getKeys();
           
           $this->validateKeys($keys, filter_input_array(INPUT_POST));
           $brand = Brand::instanciate($_POST);
          // print_r($brand);
           $response = Brands_bl::actualizar($brand);
            print_r($_POST);
            if (true) {
                $r = ["error" => 0, "msg" => "Actualizado correctamente"];
            } else {
                $r = ["error" => 1, "msg" => "No se pudo actualizar"];
            }
           
        } else {
            $r = ["error" => 1, "msg" => "Debe proveer todos los datos"];
        }
         //echo json_encode($brand);
    }

}
