<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Categories_controller extends BController{

    function __construct() {
        parent::__construct();
    }

    public function index()
    {
       $this->view->category = Categories_bl::getAll2();
       $this->view->categ = Categories_bl::getAll();
        $this->view->render($this,"index");
    }

    public function create() {
        $r = [];
        if (isset($_POST)) {
           echo "estoy en el controlador";
            $response = Categories_bl::create($_POST);
            
            if ($response) {
                $r = ["error" => 0, "msg" => "creado correctamente"];
            } else {
                $r = ["error" => 1, "msg" => "No se pudo crear"];
            }
        } else {
            $r = ["error" => 1, "msg" => "Debe proveer todos los datos"];
        }
     echo json_encode($r);
        
    }

    public function eliminarCategory() {
        $r = [];

        if (isset($_POST["id"])) {
            $id = $_POST["id"];
            
        $cat = Category::getById($id);          
            $response = Categories_bl::eliminar($cat);
            if ($response) {
                $r = ["error" => 0, "msg" => "Eliminado"];
            //$this->view->reloadThis($this);
            } else {
                $r = ["error" => 1, "msg" => "No se pudo eliminar"];
            }

        } else {
            $r = ["error" => 1, "msg" => "Debe proveer todos los datos"];
        }
        
    print(json_encode($r));
    }

    public function editarCategory() {
        $r = [];
        if (isset($_POST)) {
        $keys = Category::getKeys();
        
        $this->validateKeys($keys, filter_input_array(INPUT_POST));
        $cat = Category::instanciate($_POST);
        // print_r($category);
        $response = Categories_bl::actualizar($cat);
            print_r($_POST);
            if (true) {
                $r = ["error" => 0, "msg" => "Actualizado correctamente"];
            } else {
                $r = ["error" => 1, "msg" => "No se pudo actualizar"];
            }
        
        } else {
            $r = ["error" => 1, "msg" => "Debe proveer todos los datos"];
        }
        echo json_encode($cliente);
    }

}
