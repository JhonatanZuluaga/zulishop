<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Bundles_controller extends BController{

    function __construct() {
        parent::__construct();
    }

    public function index()
    {
        $this->view->bundles = Bundles_bl::getAll();
        $this->view->render($this,"index");
    }
    
    public function create(){
    
        $r = [];
        if (isset($_POST)) {
        

            $response = Bundles_bl::create($_POST);
            
            if ($response) {
                $r = ["error" => 0, "msg" => "creado correctamente"];
            } else {
                $r = ["error" => 1, "msg" => "No se pudo crear"];
            }
        } else {
            $r = ["error" => 1, "msg" => "Debe proveer todos los datos"];
        }
     echo json_encode($r);
    }

}
