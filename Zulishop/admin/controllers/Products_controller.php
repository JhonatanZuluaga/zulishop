<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Products_controller extends BController {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->view->products = Products_bl::getAll2();
         $this->getMyBrand();
        $this->view->render($this, "index");
    }

    public function create() {
        $r = [];
        if (isset($_POST)) {
//            $keys = Product::getKeys();
//            unset($keys[0]);
//             $_POST["id"] = null;
//              unset($keys[7]);
//             $_POST["rating"] = null;
//            $this->validateKeys($keys, filter_input_array(INPUT_POST));
            $response = Products_bl::create($_POST);

            if ($response) {
                $r = ["error" => 0, "msg" => "creado correctamente"];
            } else {
                $r = ["error" => 1, "msg" => "No se pudo crear"];
            }
        } else {
            $r = ["error" => 1, "msg" => "Debe proveer todos los datos"];
        }
        echo json_encode($r);
    }
    
     public function getMyBrand(){
      $this->view->marquitas = Brands_bl::getAll();
        }

    public function editarProducto() {
        $r = [];
        if (isset($_POST)) {
            $keys = Product::getKeys();
            
            $this->validateKeys($keys, filter_input_array(INPUT_POST));
            $prod = Product::instanciate($_POST);
            // print_r($prod);
            $response = Products_bl::actualizar($prod);
            print_r($_POST);
            if (true) {
                $r = ["error" => 0, "msg" => "Actualizado correctamente"];
            } else {
                $r = ["error" => 1, "msg" => "No se pudo actualizar"];
            }
            
        } else {
            $r = ["error" => 1, "msg" => "Debe proveer todos los datos"];
        }
            //echo json_encode($prod);
    }
    

    public function eliminarProducto() {
        $r = [];

        if (isset($_POST["id"])) {
            $id = $_POST["id"];
            
           $prod = Product::getById($id);          
            $response = Products_bl::eliminar($prod);
            if ($response) {
                $r = ["error" => 0, "msg" => "Eliminado"];
               //$this->view->reloadThis($this);
            } else {
                $r = ["error" => 1, "msg" => "No se pudo eliminar"];
            }

        } else {
            $r = ["error" => 1, "msg" => "Debe proveer todos los datos"];
        }
        
       print(json_encode($r));
    }


}
