<div class="col-xs-12">
    <br>
    <div class="box">
      <div class="box-header">
            <h3 class="box-title">Crear Nuevos Bundles</h3>

            <div class="box-tools">
                
            </div>
        </div>
     <form id="formularioCools" class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label for="rolInput" class="col-sm-2 control-label">Discount</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="rolInput" placeholder="Discount" name="discount">
                  </div>
                </div>
            
  
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Crear</button>
              </div>
              <!-- /.box-footer -->
            </form>
</div>
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Promociones de Zulishop</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody><tr>
                  <th>ID</th>
                  <th>Discount</th>
                  
                  
                </tr>
                <?php foreach ($this->bundles as $bundlese) : ?>
                <tr>
                  <td><?php print $bundlese["id"]; ?></td>
                  <td><?php print $bundlese["discount"]; ?></td>
                  
               
                </tr>
                <?php endforeach; ?>
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>

<?php $this->asyncCreation("#formularioCools","Bundles/create","Bundle","POST","Bundles/index"); ?>;