<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Recibos de Zulishop</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody><tr>
                  <th>ID</th>
                  <th>Date</th>
                  <th>Client</th>
                  <th>Total</th>
                  
                </tr>
                <?php foreach ($this->receipts as $receiptes) : ?>
                <tr>
               
                  <td><?php print $receiptes->getId(); ?></td>
                  <td><?php print $receiptes->getDate(); ?></td>
                  <td><?php print $receiptes->receiptDetail->getUsername(); ?></td>
                  <td><?php print $receiptes->getTotal(); ?></td>
                  
                  
               
                </tr>
                <?php endforeach; ?>
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>