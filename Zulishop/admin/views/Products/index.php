<div class="col-xs-12">
    <br>
     <div class="box">
      <div class="box-header">
            <h3 class="box-title">Crear Nuevos Productos</h3>

            <div class="box-tools">
                
            </div>
        </div>
     <form id="formularioCool" class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label for="rolInput" class="col-sm-2 control-label">Name</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="rolInput" placeholder="Name" name="name">
                  </div>
                </div>
                   <div class="form-group">
                  <label for="rolInput" class="col-sm-2 control-label">Description</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="rolInput" placeholder="Description" name="description">
                  </div>
                </div>
                <div class="form-group">
                  <label for="startInput" class="col-sm-2 control-label">Price</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="startInput" placeholder="Price" name="price">
                  </div>
                </div>
                  
                    <div class="form-group">
                  <label for="startInput" class="col-sm-2 control-label">Quantity</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="startInput" placeholder="Quantity" name="quantity">
                  </div>
                </div>
                  
                   <div class="form-group">
                  <label for="startInput" class="col-sm-2 control-label">Brand</label>

                  <div class="col-sm-10">
                    <select name="brand" id="hijo">
                       <option value="null">Seleccione marca</option>
                       <?php foreach($this->marquitas as $cat) : ?>
                      <option value="<?php print $cat["id"]; ?>"><?php print $cat["name"]; ?></option>
                    <?php endforeach; ?>
                  </select>
                  </div>
                </div>
                  
                   <div class="form-group">
                  <label for="startInput" class="col-sm-2 control-label">Disscount</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="startInput" placeholder="Discount" name="disscount">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Crear</button>
              </div>
              <!-- /.box-footer -->
            </form>
</div>
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Productos de Zulishop</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody id="dataUpdate"><tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Brand</th>
                  <th>Disccount</th>
                </tr>
                <?php foreach ($this->products as $product) : ?>
                <tr>
                  <td><input class="<?php print $product->getId(); ?> id" type="text" name="id" value="<?php print $product->getId(); ?>" required disabled></td>
                  <td><input class="<?php print $product->getId(); ?>" type="text" name="name" value="<?php print $product->getName(); ?>" required disabled></td>
                  <td><input class="<?php print $product->getId(); ?>" type="text" name="price" value="<?php print $product->getPrice(); ?>" required disabled></td>
                  <td><input class="<?php print $product->getId(); ?>" type="text" name="quantity" value="<?php print $product->getQuantity(); ?>" required disabled></td>
                  <!-- <td><input class="<?php print $product->getId(); ?>" type="text" name="brand" value="<?php print $product->marcaDetail->getName(); ?>" required disabled></td> -->
                  <td>
                    <select name="brand" class="<?php print $product->getId(); ?>" disabled>
                        <?php foreach($this->marquitas as $brand) : ?>
                            <?php if($brand['id'] == $product->getBrand()) :?>
                                <option value="<?php print $brand["id"]; ?>" selected><?php print $brand["name"]; ?></option>
                            <?php else : ?>
                                <option value="<?php print $brand["id"]; ?>"><?php print $brand["name"]; ?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
                  </td>
                  <td><input class="<?php print $product->getId(); ?>" type="text" name="disscount" value="<?php print $product->getDisscount(); ?>" required disabled></td>
                  <td style="display:none"><input class="<?php print $product->getId(); ?>" type="text" name="description" value="<?php print $product->getDescription(); ?>" required disabled></td>
                    <td>
                        <button type="submit" class="btn btn-default edit" data-status="disable" id="<?php print $product->getId(); ?>"><i class="fa fa-edit btnEdit">Editar</i></button>
                    </td>
                    <td>
                        <button type="button" class="btn btn-default delet" id="<?php print $product->getId(); ?>"><i class="">Eliminar</i></button>
                    </td>
                </tr>
                <?php endforeach; ?>
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>

<?php $this->asyncCreation("#formularioCool","Products/create","Producto","POST","Products/index"); ?>;



<script type="text/javascript">


//iniciamos la asignación de listeners
addListenersToElements();

//Función nueva! :D
function addListenersToElements() {
    //capturar los elementos
    var elementos = document.querySelectorAll(".delet");
    //recorremos el arreglo de elementos
    //console.log("me presiono");
    for (var i = 0; i < elementos.length; i++) {
        addElementListenners(elementos[i]);
    }
}

addListenersEdit();
//Función nueva! :D

function addListenersEdit() {
    //capturar los elementos
    var elementos = document.querySelectorAll(".edit");
    //recorremos el arreglo de elementos
    //console.log("me presiono 2");
    for (var i = 0; i < elementos.length; i++) {
        addElementListennersEdit(elementos[i]);
    }
}

    function addElementListennersEdit(elemento) {

        elemento.onclick = function () {
            
            var id = this.id;

            if (this.dataset.status != "edit") {
                $("." + id+":not('.id')").prop('disabled', false);
                $("#" + id + " .btnEdit").text("Enviar");
                this.dataset.status = "edit";
                
            } else {
                $("." + id+":not('.id')").prop('disabled', true);
                $("#" + id + " .btnEdit").text("Editar");
                this.dataset.status = "disable";

                var data = {};
                var inputs = $("#dataUpdate").find("input."+id);
                data['brand'] = $("#dataUpdate").find("select."+id).val();
                data['rating']= 'null';
                $.each(inputs, function() {
                    if(this.name == 'disscount' && this.value == 0){
                        data[this.name] = 'null';                        
                    }else{
                        data[this.name] = this.value;
                    }
                });

                $.ajax({
                url: "<?php print(URL); ?>Products/editarProducto",
                method: "POST",
                data: data
                }).done(function (r) {
                    console.log(r);
                    //var r = JSON.parse(r);
                    if (r.error) {
                        alert(r.msg);
                    } else {
                        <?php $this->reloadThis("Products/index"); ?>;
                    }
                });
            }
        }
    }
    
      $("#formularioCool").submit(function(e){
                e.preventDefault();
                var form = $(this)[0];
                console.log(form);
               var data = {};
               var inputs =  form.querySelectorAll("input");
               console.log(inputs);
               data = processForm(form);
               console.log(data);
               data['rol'] = $("#papa").find("#hijo").val();
               $.ajax({
                   url:"<?php print(URL); ?>Products/create",
                   method: "POST",
                   data: data
               }).done(function(r){
                   //var r = JSON.parse(r);
                    if (r.error) {
                        alert(r.msg);
                    } else {
                        <?php $this->reloadThis("Products/index"); ?>;
                    }
                   
               });
             
            });
            
                function processForm(form){
                var inputs = form.querySelectorAll("input");
                var data = {};
                $(inputs).each(function(){
                    var input = $(this);
                    if(input.context.type != "submit"){
                        var attr = input.context.name;
                        data[attr] = input.context.value;
    
                    }
                });
                return data;
            }



function addElementListenners(elemento) {

    elemento.onclick = function () {
        var id = this.id;
        if (id != null) {
            var data = {
                id: id
            };
            $.ajax({
                url: "<?php print(URL); ?>Products/eliminarProducto",
                method: "POST",
                data: data
            }).done(function (r) {
                console.log(r);
                var r = JSON.parse(r);
                if (r.error) {
                    alert(r.msg);
                } else {
                    alert(r.msg);
                    <?php $this->reloadThis("Products/index"); ?>;
                }
            });
        }
    }
}

</script>