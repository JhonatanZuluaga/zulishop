<div class="col-xs-12">
    <br>
    <div class="box">
      <div class="box-header">
            <h3 class="box-title">Crear Nuevos Provedores</h3>

            <div class="box-tools">
                
            </div>
        </div>
     <form id="formularioCools" class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label for="rolInput" class="col-sm-2 control-label">Name</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="rolInput" placeholder="Name" name="name">
                  </div>
                </div>
                   <div class="form-group">
                  <label for="rolInput" class="col-sm-2 control-label">Tel</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="rolInput" placeholder="Tel" name="tel">
                  </div>
                </div>
                <div class="form-group">
                  <label for="startInput" class="col-sm-2 control-label">Email</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="startInput" placeholder="Email" name="email">
                  </div>
                </div>
                  
                    <div class="form-group">
                  <label for="startInput" class="col-sm-2 control-label">Address</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="startInput" placeholder="Address" name="address">
                  </div>
                </div>
              
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Crear</button>
              </div>
              <!-- /.box-footer -->
            </form>
</div>
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Provedores de Zulishop</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody id="dataUpdate"><tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Phone</th>
                  <th>Email</th>
                  <th>Address</th>
                  
                </tr>
                <?php foreach ($this->provides as $provides) : ?>
                <tr>
                  <td><input class="<?php print $provides['id'] ?> id" type="text" name="id" value="<?php print $provides['id'] ?>" required disabled></td>
                  <td><input class="<?php print $provides['id'] ?>" type="text" name="name" value="<?php print $provides['name'] ?>" required disabled></td>
                  <td><input class="<?php print $provides['id'] ?>" type="text" name="tel" value="<?php print $provides['tel'] ?>" required disabled></td>
                  <td><input class="<?php print $provides['id'] ?>" type="text" name="email" value="<?php print $provides['email'] ?>" required disabled></td>
                  <td><input class="<?php print $provides['id'] ?>" type="text" name="address" value="<?php print $provides['address'] ?>" required disabled></td>
                  <td>
                      <button type="submit" class="btn btn-default edit" data-status="disable" id="<?php print $provides['id'] ?>"><i class="fa fa-edit btnEdit">Editar</i></button>
                  </td>
                  <td>
                      <button type="button" class="btn btn-default delet" id="<?php print $provides['id'] ?>"><i class="">Eliminar</i></button>
                  </td>
                  
                </tr>
                <?php endforeach; ?>
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>

<?php $this->asyncCreation("#formularioCools","Providers/create","Producto","POST","Providers/index"); ?>;

<script type="text/javascript">


//iniciamos la asignación de listeners
addListenersToElements();

//Función nueva! :D
function addListenersToElements() {
    //capturar los elementos
    var elementos = document.querySelectorAll(".delet");
    //recorremos el arreglo de elementos
    //console.log("me presiono");
    for (var i = 0; i < elementos.length; i++) {
        addElementListenners(elementos[i]);
    }
}

addListenersEdit();
//Función nueva! :D

function addListenersEdit() {
    //capturar los elementos
    var elementos = document.querySelectorAll(".edit");
    //recorremos el arreglo de elementos
    //console.log("me presiono 2");
    for (var i = 0; i < elementos.length; i++) {
        addElementListennersEdit(elementos[i]);
    }
}

    function addElementListennersEdit(elemento) {

        elemento.onclick = function () {
            
            var id = this.id;

            if (this.dataset.status != "edit") {
                $("." + id+":not('.id')").prop('disabled', false);
                $("#" + id + " .btnEdit").text("Enviar");
                this.dataset.status = "edit";
                
            } else {
                $("." + id+":not('.id')").prop('disabled', true);
                $("#" + id + " .btnEdit").text("Editar");
                this.dataset.status = "disable";

                var data = {};
                var inputs = $("#dataUpdate").find("input."+id);
                data['rol'] = $("#dataUpdate").find("select."+id).val();
                $.each(inputs, function() {
                    data[this.name] = this.value;
                    console.log(data);
                });

                $.ajax({
                url: "<?php print(URL); ?>Providers/editarProvider",
                method: "POST",
                data: data
                }).done(function (r) {
                    console.log(r);
                    //var r = JSON.parse(r);
                    if (r.error) {
                        alert(r.msg);
                    } else {
                        <?php $this->reloadThis("Providers/index"); ?>;
                    }
                });
            }
        }
    }

function addElementListenners(elemento) {

    elemento.onclick = function () {
        var id = this.id;
        if (id != null) {
            var data = {
                id: id
            };
            $.ajax({
                url: "<?php print(URL); ?>Providers/eliminarProvider",
                method: "POST",
                data: data
            }).done(function (r) {
                console.log(r);
                var r = JSON.parse(r);
                if (r.error) {
                    alert(r.msg);
                } else {
                    alert(r.msg);
                    <?php $this->reloadThis("Providers/index"); ?>;
                }
            });
        }
    }
}

</script>