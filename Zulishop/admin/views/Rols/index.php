</br>

<div class="col-xs-12">
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Crear Nuevos Roles</h3>

            <div class="box-tools">

            </div>
        </div>
        <form id="formularioCools" class="form-horizontal">
            <div class="box-body">
                <div class="form-group">
                    <label for="rolInput" class="col-sm-2 control-label">Rol</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="rolInput" placeholder="Rol" name="rol">
                    </div>
                </div>


            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Crear</button>
            </div>
            <!-- /.box-footer -->
        </form>
    </div>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Roles - Zulishop</h3>

            <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>

        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table class="table table-hover">
                <tbody id="dataUpdate"><tr>
                        <th>ID</th>
                        <th>Rol</th>


                    </tr>
                    <?php foreach ($this->roles as $rols) : ?>
                        <tr>
                            <td><input class="<?php print $rols['id'] ?> id" type="text" name="id" value="<?php print $rols['id'] ?>" required disabled></td>
                            <td><input class="<?php print $rols['id'] ?>" type="text" name="rol" value="<?php print $rols['rol'] ?>" required disabled></td>
                            <td>
                                <button type="submit" class="btn btn-default edit" data-status="disable" id="<?php print $rols['id'] ?>"><i class="fa fa-edit btnEdit">Editar</i></button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-default delet" id="<?php print $rols['id'] ?>"><i class="">Eliminar</i></button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody></table>
        </div>
        <!-- /.box-body -->
    </div>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Orden de Items - Zulishop</h3>

            <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>

        

        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table class="table table-hover">
                    </tr>
                    <?php foreach ($this->roles as $rols) : ?>
                    <div class="panel panel-default col-sm-6 nopadding">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" href="#panel-filter-brand">
                                    <input class="<?php print $rols['id'] ?>" type="text" name="rol" value="<?php print $rols['rol'] ?>" required disabled>
                                </a>
                            </h4>
                        </div>
                        <div id="panel-filter-brand" class="accordion-body collapse in">
                            <div class="panel-body">
                            <ul class="items">
                                    <li class="draggable" draggable="true">item</li>
                                    <li class="draggable" draggable="true">item</li>
                                    <li class="draggable" draggable="true">item</li>
                                    <li class="draggable" draggable="true">item</li>
                            </ul>
                            </div>
                        </div>
                        </div>
                    <?php endforeach; ?>
            </table>

            
        </div>
    </table>
        </div>
        <!-- /.box-body -->
    </div>
</div>


<?php $this->asyncCreation("#formularioCools", "Rols/create", "Rol", "POST", "Rols/index"); ?>;

<script type="text/javascript">


//iniciamos la asignación de listeners
    addListenersToElements();

//Función nueva! :D
    function addListenersToElements() {
        //capturar los elementos
        var elementos = document.querySelectorAll(".delet");
        //recorremos el arreglo de elementos
        //console.log("me presiono");
        for (var i = 0; i < elementos.length; i++) {
            addElementListenners(elementos[i]);
        }
    }

    addListenersEdit();
//Función nueva! :D

    function addListenersEdit() {
        //capturar los elementos
        var elementos = document.querySelectorAll(".edit");
        //recorremos el arreglo de elementos
        //console.log("me presiono 2");
        for (var i = 0; i < elementos.length; i++) {
            addElementListennersEdit(elementos[i]);
        }
    }

    function addElementListennersEdit(elemento) {

        elemento.onclick = function () {

            var id = this.id;

            if (this.dataset.status != "edit") {
                $("." + id + ":not('.id')").prop('disabled', false);
                $("#" + id + " .btnEdit").text("Enviar");
                this.dataset.status = "edit";

            } else {
                $("." + id + ":not('.id')").prop('disabled', true);
                $("#" + id + " .btnEdit").text("Editar");
                this.dataset.status = "disable";

                var data = {};
                var inputs = $("#dataUpdate").find("input." + id);
                $.each(inputs, function () {
                    data[this.name] = this.value;
                    console.log(data);
                });

                $.ajax({
                    url: "<?php print(URL); ?>Rols/editarRol",
                    method: "POST",
                    data: data
                }).done(function (r) {
                    console.log(r);
                    //var r = JSON.parse(r);
                    if (r.error) {
                        alert(r.msg);
                    } else {
<?php $this->reloadThis("Rols/index"); ?>;
                    }
                });
            }
        }
    }

    function addElementListenners(elemento) {

        elemento.onclick = function () {
            var id = this.id;
            if (id != null) {
                var data = {
                    id: id
                };
                $.ajax({
                    url: "<?php print(URL); ?>Rols/eliminarRol",
                    method: "POST",
                    data: data
                }).done(function (r) {
                    console.log(r);
                    var r = JSON.parse(r);
                    if (r.error) {
                        alert(r.msg);
                    } else {
                        alert(r.msg);
<?php $this->reloadThis("Rols/index"); ?>;
                    }
                });
            }
        }
    }

///////////////////

    var remove = document.querySelector('.draggable');

    function dragStart(e) {
        this.style.opacity = '0.4';
        dragSrcEl = this;
        e.dataTransfer.effectAllowed = 'move';
        e.dataTransfer.setData('text/html', this.innerHTML);
    }
    ;

    function dragEnter(e) {
        this.classList.add('over');
    }

    function dragLeave(e) {
        e.stopPropagation();
        this.classList.remove('over');
    }

    function dragOver(e) {
        e.preventDefault();
        e.dataTransfer.dropEffect = 'move';
        return false;
    }

    function dragDrop(e) {
        if (dragSrcEl != this) {
            dragSrcEl.innerHTML = this.innerHTML;
            this.innerHTML = e.dataTransfer.getData('text/html');
        }
        return false;
    }

    function dragEnd(e) {
        var listItens = document.querySelectorAll('.draggable');
        [].forEach.call(listItens, function (item) {
            item.classList.remove('over');
        });
        this.style.opacity = '1';
    }

    function addEventsDragAndDrop(el) {
        el.addEventListener('dragstart', dragStart, false);
        el.addEventListener('dragenter', dragEnter, false);
        el.addEventListener('dragover', dragOver, false);
        el.addEventListener('dragleave', dragLeave, false);
        el.addEventListener('drop', dragDrop, false);
        el.addEventListener('dragend', dragEnd, false);
    }

    var listItens = document.querySelectorAll('.draggable');
    [].forEach.call(listItens, function (item) {
        addEventsDragAndDrop(item);
    });

</script>