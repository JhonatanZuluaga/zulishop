<div class="col-xs-12">
     <div class="box">
        <div class="box-header">
            <h3 class="box-title">Crear Usuarios de ZuliShop</h3>

            <div class="box-tools">

            </div>
        </div>
        <form id="formularioCool" class="form-horizontal">
            <div class="box-body">
                <div class="form-group">
                    <label for="rolInput" class="col-sm-2 control-label">Username</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="rolInput" placeholder="Username" name="username">
                    </div>
                </div>
                <div class="form-group">
                    <label for="startInput" class="col-sm-2 control-label">Password</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="startInput" placeholder="Password" name="password">
                    </div>
                </div>

                <div class="form-group">
                    <label for="startInput" class="col-sm-2 control-label">Email</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="startInput" placeholder="Email" name="email">
                    </div>
                </div>
                
                <div class="form-group" id="papa">
                    <label for="startInput" class="col-sm-2 control-label">Rol</label>
                    
                    <select name="rol" class="select" id="hijo" >
                                <?php foreach($this->roles as $rol) : ?>
                                  
                                        <option value="<?php print $rol['id'] ?>" ><?php print $rol['rol']?></option>
                                   
                                <?php endforeach; ?>
                            </select>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Crear</button>
            </div>
            <!-- /.box-footer -->
        </form>
    </div>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Usuarios de ZuliShop</h3>

            <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tbody id="dataUpdate"><tr>
                        <th>ID</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Rol</th>
                    </tr>
                    <?php foreach ($this->users as $user) : ?>
                        <tr>
                    <!-- <form id="dataUpdate" autocomplete="off"> -->
                        <td><input class="<?php print $user->getId(); ?> id" type="text" name="id" value="<?php print $user->getId(); ?>" required placeholder="Username" disabled></td>
                        <td><input class="<?php print $user->getId(); ?>" type="text" name="username" value="<?php print $user->getUsername(); ?>" required placeholder="Username" disabled></td>
                        <td style="display:none"><input class="<?php print $user->getId(); ?>" type="text" name="password" value="<?php print $user->getPassword(); ?>" required placeholder="Username" disabled style="display:none"></td>
                        <td><input class="<?php print $user->getId(); ?>" type="text" name="email" value="<?php print $user->getEmail(); ?>" required placeholder="Username" disabled></td>
                        <td>
                            <select name="rol" class="<?php print $user->getId(); ?>" disabled>
                                <?php foreach($this->roles as $rol) : ?>
                                    <?php if($rol['id'] == $user->getRol()) :?>
                                        <option value="<?php print $rol['id'] ?>" selected><?php print $rol['rol']?></option>
                                    <?php else : ?>
                                        <option value="<?php print $rol['id'] ?>" ><?php print $rol['rol']?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </td>
                        <!-- <input class="<?php print $user->getId(); ?>" type="text" name="rol" value="<?php print $user->rolDetail->getRol(); ?>" required placeholder="Username" disabled> -->
                        <td>
                            <button type="submit" class="btn btn-default edit" data-status="disable" id="<?php print $user->getId(); ?>"><i class="fa fa-edit btnEdit">Editar</i></button>
                        </td>
                        <td>
                            <button type="button" class="btn btn-default delet" id="<?php print $user->getId(); ?>"><i class="">Eliminar</i></button>
                        </td>
                    <!-- </form> -->
                    </tr>
                    <?php endforeach; ?>
                </tbody></table>

        </div>



        <!-- Modal -->

        <!--            <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog">
        
                             Modal content
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Modal Header</h4>
                                </div>
                                <div class="modal-body">
                                    <form name="updateForm" autocomplete="off">
                                        <input type="text" name="username" value="" required placeholder="Username">
                                        <input type="email" name="email" value="" required placeholder="email">
                                        <input type="password" name="password" value="" required placeholder="Password">
                                        <input type="password" name="passwordR" value="" required placeholder="Repeat Password">
                                        <input type="submit" value="Registrar">
        
                                    </form>
        
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
        
                        </div>
                    </div>-->


        <!-- /.box-body -->
    </div>
    <!-- /.box -->
   
</div>

<script type="text/javascript">


//iniciamos la asignación de listeners
addListenersToElements();

//Función nueva! :D
function addListenersToElements() {
    //capturar los elementos
    var elementos = document.querySelectorAll(".delet");
    //recorremos el arreglo de elementos
    //console.log("me presiono");
    for (var i = 0; i < elementos.length; i++) {
        addElementListenners(elementos[i]);
    }
}

addListenersEdit();
//Función nueva! :D

function addListenersEdit() {
    //capturar los elementos
    var elementos = document.querySelectorAll(".edit");
    //recorremos el arreglo de elementos
    //console.log("me presiono 2");
    for (var i = 0; i < elementos.length; i++) {
        addElementListennersEdit(elementos[i]);
    }
}

    function addElementListennersEdit(elemento) {

        elemento.onclick = function () {
            
            var id = this.id;

            if (this.dataset.status != "edit") {
                $("." + id+":not('.id')").prop('disabled', false);
                $("#" + id + " .btnEdit").text("Enviar");
                this.dataset.status = "edit";
                
            } else {
                $("." + id+":not('.id')").prop('disabled', true);
                $("#" + id + " .btnEdit").text("Editar");
                this.dataset.status = "disable";

                var data = {};
                var inputs = $("#dataUpdate").find("input."+id);
                data['rol'] = $("#dataUpdate").find("select."+id).val();
                $.each(inputs, function() {
                    data[this.name] = this.value;
                    console.log(data);
                });

                $.ajax({
                url: "<?php print(URL); ?>Users/editarUsuario",
                method: "POST",
                data: data
                }).done(function (r) {
                    console.log(r);
                    //var r = JSON.parse(r);
                    if (r.error) {
                        alert(r.msg);
                    } else {
                        <?php $this->reloadThis("Users/index"); ?>;
                    }
                });
            }
        }
    }
    
      $("#formularioCool").submit(function(e){
                e.preventDefault();
                var form = $(this)[0];
                console.log(form);
               var data = {};
               var inputs =  form.querySelectorAll("input");
               console.log(inputs);
               data = processForm(form);
               console.log(data);
               data['rol'] = $("#papa").find("#hijo").val();
               $.ajax({
                   url:"<?php print(URL); ?>Users/create",
                   method: "POST",
                   data: data
               }).done(function(r){
                   //var r = JSON.parse(r);
                    if (r.error) {
                        alert(r.msg);
                    } else {
                        <?php $this->reloadThis("Users/index"); ?>;
                    }
                   
               });
             
            });
            
                function processForm(form){
                var inputs = form.querySelectorAll("input");
                var data = {};
                $(inputs).each(function(){
                    var input = $(this);
                    if(input.context.type != "submit"){
                        var attr = input.context.name;
                        data[attr] = input.context.value;
    
                    }
                });
                return data;
            }



function addElementListenners(elemento) {

    elemento.onclick = function () {
        var id = this.id;
        if (id != null) {
            var data = {
                id: id
            };
            $.ajax({
                url: "<?php print(URL); ?>Users/eliminarUsuario",
                method: "POST",
                data: data
            }).done(function (r) {
                console.log(r);
                var r = JSON.parse(r);
                if (r.error) {
                    alert(r.msg);
                } else {
                    alert(r.msg);
                    <?php $this->reloadThis("Users/index"); ?>;
                }
            });
        }
    }
}

</script>