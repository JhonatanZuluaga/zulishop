<div class="col-xs-12">
  <!-- /.box -->
  <div class="box">
    <div class="box-header">
        <h3 class="box-title">Crear Categorías de ZuliShop</h3>

        <div class="box-tools">

        </div>
    </div>
    <form id="formularioCool" class="form-horizontal">
        <div class="box-body">
            <div class="form-group">
                <label for="nameInput" class="col-sm-2 control-label">Name</label>

                <div class="col-sm-10">
                    <input type="text" class="form-control" id="nameInput" placeholder="Name" name="name">
                </div>
            </div>
            <div class="form-group" id="papa">
                <label for="parentSelect" class="col-sm-2 control-label">Parent</label>
                <div class="col-sm-10">
                  <select name="parent" id="hijo">
                       <option value="null">Sin parent</option>
                       <?php foreach($this->category as $cat) : ?>
                      <option value="<?php print $cat->getId(); ?>"><?php print $cat->getName();?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" class="btn btn-info pull-right">Crear</button>
        </div>
        <!-- /.box-footer -->
    </form>
  </div>
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Categorias de Zulishop</h3>

      <div class="box-tools">
        <div class="input-group input-group-sm" style="width: 150px;">
          <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

          <div class="input-group-btn">
            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive no-padding">
      <table class="table table-hover">
        <tbody id="dataUpdate"><tr>
          <th>ID</th>
          <th>Name</th>
          <th>Parent</th>
          
        </tr>
        <?php foreach ($this->categ as $categories) : ?>
        
        <tr>
        <td><input class="<?php print $categories['id'] ?> id" type="text" name="id" value="<?php print $categories['id'] ?>" required disabled></td>
        <td><input class="<?php print $categories['id'] ?>" type="text" name="name" value="<?php print $categories['name'] ?>" required disabled></td>
        <td><input class="<?php print $categories['id'] ?>" type="text" name="parent" value="<?php print $categories['parent'] ?>" required disabled></td>
          <td>
              <button type="submit" class="btn btn-default edit" data-status="disable" id="<?php print $categories['id'] ?>"><i class="fa fa-edit btnEdit">Editar</i></button>
          </td>
          <td>
              <button type="button" class="btn btn-default delet" id="<?php print $categories['id'] ?>"><i class="">Eliminar</i></button>
          </td>
         
        </tr>
        
        <?php endforeach; ?>
      </tbody></table>
    </div>
    <!-- /.box-body -->
  </div>
</div>
  
</div>
    <!-- /.box -->




<script type="text/javascript">
    
$("#formularioCool").submit(function(e){
                e.preventDefault();
                var form = $(this)[0];
                console.log(form);
               var data = {};
               var inputs =  form.querySelectorAll("input");
               console.log(inputs);
               data = processForm(form);
               console.log(data);
               data['parent'] = $("#papa").find("#hijo").val();
               $.ajax({
                   url:"<?php print(URL); ?>Categories/create",
                   method: "POST",
                   data: data
               }).done(function(r){
                   //var r = JSON.parse(r);
                    if (r.error) {
                        alert(r.msg);
                    } else {
                        <?php $this->reloadThis("Categories/index"); ?>;
                    }
                   
               });
             
            });
            
                function processForm(form){
                var inputs = form.querySelectorAll("input");
                var data = {};
                $(inputs).each(function(){
                    var input = $(this);
                    if(input.context.type != "submit"){
                        var attr = input.context.name;
                        data[attr] = input.context.value;
    
                    }
                });
                return data;
            }

//iniciamos la asignación de listeners
addListenersToElements();

//Función nueva! :D
function addListenersToElements() {
    //capturar los elementos
    var elementos = document.querySelectorAll(".delet");
    //recorremos el arreglo de elementos
    //console.log("me presiono");
    for (var i = 0; i < elementos.length; i++) {
        addElementListenners(elementos[i]);
    }
}

addListenersEdit();
//Función nueva! :D

function addListenersEdit() {
    //capturar los elementos
    var elementos = document.querySelectorAll(".edit");
    //recorremos el arreglo de elementos
    //console.log("me presiono 2");
    for (var i = 0; i < elementos.length; i++) {
        addElementListennersEdit(elementos[i]);
    }
}

    function addElementListennersEdit(elemento) {

        elemento.onclick = function () {
            
            var id = this.id;

            if (this.dataset.status != "edit") {
                $("." + id+":not('.id')").prop('disabled', false);
                $("#" + id + " .btnEdit").text("Enviar");
                this.dataset.status = "edit";
                
            } else {
                $("." + id+":not('.id')").prop('disabled', true);
                $("#" + id + " .btnEdit").text("Editar");
                this.dataset.status = "disable";

                var data = {};
                var inputs = $("#dataUpdate").find("input."+id);
                $.each(inputs, function() {
                    if(this.name == 'parent' && this.value == ''){
                        data[this.name] = 1;                        
                    }else{
                        data[this.name] = this.value;
                    }
                    console.log(data);
                });

                $.ajax({
                url: "<?php print(URL); ?>Categories/editarCategory",
                method: "POST",
                data: data
                }).done(function (r) {
                    console.log(r);
                    //var r = JSON.parse(r);
                    if (r.error) {
                        alert(r.msg);
                    } else {
                        <?php $this->reloadThis("Categories/index"); ?>;
                    }
                });
            }
        }
    }

function addElementListenners(elemento) {

    elemento.onclick = function () {
        var id = this.id;
        if (id != null) {
            var data = {
                id: id
            };
            $.ajax({
                url: "<?php print(URL); ?>Categories/eliminarCategory",
                method: "POST",
                data: data
            }).done(function (r) {
                console.log(r);
                var r = JSON.parse(r);
                if (r.error) {
                    alert(r.msg);
                } else {
                    alert(r.msg);
                    <?php $this->reloadThis("Categories/index"); ?>;
                }
            });
        }
    }
}



</script>
    