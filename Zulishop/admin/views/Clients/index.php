<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Clientes de Zulishop</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody><tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  
                </tr>
                <?php foreach ($this->clients as $clientes) : ?>
                <tr>
                <td><input class="<?php print $clientes['id'] ?> id" type="text" name="id" value="<?php print $clientes['id'] ?>" required disabled></td>
                <td><input class="<?php print $clientes['id'] ?>" type="text" name="username" value="<?php print $clientes['username'] ?>" required disabled></td>
                <td><input class="<?php print $clientes['id'] ?>" type="text" name="email" value="<?php print $clientes['email'] ?>" required disabled></td>
                  <td>
                      <button type="submit" class="btn btn-default edit" data-status="disable" id="<?php print $clientes['id'] ?>"><i class="fa fa-edit btnEdit">Editar</i></button>
                  </td>
                  <td>
                      <button type="button" class="btn btn-default delet" id="<?php print $clientes['id'] ?>"><i class="">Eliminar</i></button>
                  </td>
                </tr>
                <?php endforeach; ?>
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>

<script type="text/javascript">


//iniciamos la asignación de listeners
addListenersToElements();

//Función nueva! :D
function addListenersToElements() {
    //capturar los elementos
    var elementos = document.querySelectorAll(".delet");
    //recorremos el arreglo de elementos
    //console.log("me presiono");
    for (var i = 0; i < elementos.length; i++) {
        addElementListenners(elementos[i]);
    }
}

addListenersEdit();
//Función nueva! :D

function addListenersEdit() {
    //capturar los elementos
    var elementos = document.querySelectorAll(".edit");
    //recorremos el arreglo de elementos
    //console.log("me presiono 2");
    for (var i = 0; i < elementos.length; i++) {
        addElementListennersEdit(elementos[i]);
    }
}

    function addElementListennersEdit(elemento) {

        elemento.onclick = function () {
            
            var id = this.id;

            if (this.dataset.status != "edit") {
                $("." + id+":not('.id')").prop('disabled', false);
                $("#" + id + " .btnEdit").text("Enviar");
                this.dataset.status = "edit";
                
            } else {
                $("." + id+":not('.id')").prop('disabled', true);
                $("#" + id + " .btnEdit").text("Editar");
                this.dataset.status = "disable";

                var data = {};
                var inputs = $("#dataUpdate").find("input."+id);
                $.each(inputs, function() {
                    data[this.name] = this.value;
                    console.log(data);
                });

                $.ajax({
                url: "<?php print(URL); ?>Clients/editarClient",
                method: "POST",
                data: data
                }).done(function (r) {
                    console.log(r);
                    //var r = JSON.parse(r);
                    if (r.error) {
                        alert(r.msg);
                    } else {
                        <?php $this->reloadThis("Clients/index"); ?>;
                    }
                });
            }
        }
    }

function addElementListenners(elemento) {

    elemento.onclick = function () {
        var id = this.id;
        if (id != null) {
            var data = {
                id: id
            };
            $.ajax({
                url: "<?php print(URL); ?>Clients/eliminarClient",
                method: "POST",
                data: data
            }).done(function (r) {
                console.log(r);
                var r = JSON.parse(r);
                if (r.error) {
                    alert(r.msg);
                } else {
                    alert(r.msg);
                    <?php $this->reloadThis("Clients/index"); ?>;
                }
            });
        }
    }
}

</script>