<div class="col-xs-12">
    <br>
    <div class="box">
      <div class="box-header">
            <h3 class="box-title">Crear Nuevas Marcas</h3>

            <div class="box-tools">
                
            </div>
        </div>
     <form id="formularioCools" class="form-horizontal" accept-charset="utf-8" method="POST" enctype="multipart/form-data" target="fakePage">
              <div class="box-body">
                <div class="form-group">
                  <label for="rolInput" class="col-sm-2 control-label">Name</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="rolInput" placeholder="Name" name="name">
                  </div>
                </div>
              </div>
         
            <div class="box-body">
                <div class="form-group">
                  <label for="rolInput" class="col-sm-2 control-label">Logo</label>

                  <div class="col-sm-10">
                    <input type="file" class="form-control" id="rolInput"  name="logo"  id="imagenEd1" data-target="imgEdit1" data-imgid="" data-multiple-caption="{count} files selected" multiple >
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Crear</button>
              </div>
              <!-- /.box-footer -->
              
            </form>
<!--        <iframe name="fakePage" width="100px" height="100px" src="../public/dist/img/avatar.png"></iframe>-->
</div>
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Marcas de Zulishop</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody id="dataUpdate"><tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Logo</th>
                  
                </tr>
                <?php foreach ($this->brands as $brandes) : ?>
                <tr>
                  <td><input class="<?php print $brandes["id"]; ?> id" type="text" name="id" value="<?php print $brandes["id"]; ?>" required disabled></td>
                  <td><input class="<?php print $brandes["id"]; ?>" type="text" name="name" value="<?php print $brandes["name"]; ?>" required disabled></td>
                  <td style="display:none"><input class="<?php print $brandes["id"]; ?>" type="text" name="logo" value="<?php print $brandes["logo"]; ?>" required disabled></td>
                  <td><img width="30px" height="30px" src="<?php print URL.'public/dist/img/brands/'.$brandes["logo"];?>" alt="logo"></td>

                  <td>
                      <button type="submit" class="btn btn-default edit" data-status="disable" id="<?php print $brandes["id"]; ?>"><i class="fa fa-edit btnEdit">Editar</i></button>
                  </td>
                  <td>
                      <button type="button" class="btn btn-default delet" id="<?php print $brandes["id"]; ?>"><i class="">Eliminar</i></button>
                  </td>

                </tr>
                <?php endforeach; ?>
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>

 <script type="text/javascript">
        $("#formularioCools").submit(function(e) {
          e.preventDefault();
          var formData = new FormData(document.getElementById("formularioCools"));
          $.ajax({
            type: "POST",
            dataType: "HTML",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            url: "<?php print(URL); ?>Brands/create",
          }).done(function(r) {
            console.log(r);
                    var r = JSON.parse(r);
                    if (r.error) {
                        alert(r.msg);
                    } else {
                        alert(r.msg);
                        <?php $this->reloadThis("Brands/index"); ?>;
                    }
       
          });
        });


        //iniciamos la asignación de listeners
addListenersToElements();

//Función nueva! :D
function addListenersToElements() {
    //capturar los elementos
    var elementos = document.querySelectorAll(".delet");
    //recorremos el arreglo de elementos
    //console.log("me presiono");
    for (var i = 0; i < elementos.length; i++) {
        addElementListenners(elementos[i]);
    }
}

addListenersEdit();
//Función nueva! :D

function addListenersEdit() {
    //capturar los elementos
    var elementos = document.querySelectorAll(".edit");
    //recorremos el arreglo de elementos
    //console.log("me presiono 2");
    for (var i = 0; i < elementos.length; i++) {
        addElementListennersEdit(elementos[i]);
    }
}

    function addElementListennersEdit(elemento) {

        elemento.onclick = function () {
            
            var id = this.id;

            if (this.dataset.status != "edit") {
                $("." + id+":not('.id')").prop('disabled', false);
                $("#" + id + " .btnEdit").text("Enviar");
                this.dataset.status = "edit";
                
            } else {
                $("." + id+":not('.id')").prop('disabled', true);
                $("#" + id + " .btnEdit").text("Editar");
                this.dataset.status = "disable";

                var data = {};
                var inputs = $("#dataUpdate").find("input."+id);
                
                $.each(inputs, function() {
                    data[this.name] = this.value;
                    console.log(data);
                });

                $.ajax({
                url: "<?php print(URL); ?>Brands/editarBrand",
                method: "POST",
                data: data
                }).done(function (r) {
                    console.log(r);
                    //var r = JSON.parse(r);
                    if (r.error) {
                        alert(r.msg);
                    } else {
                        <?php $this->reloadThis("Brands/index"); ?>;
                    }
                });
            }
        }
    }

function addElementListenners(elemento) {

    elemento.onclick = function () {
        var id = this.id;
        if (id != null) {
            var data = {
                id: id
            };
            $.ajax({
                url: "<?php print(URL); ?>Brands/eliminarBrand",
                method: "POST",
                data: data
            }).done(function (r) {
                console.log(r);
                var r = JSON.parse(r);
                if (r.error) {
                    alert(r.msg);
                } else {
                    alert(r.msg);
                    <?php $this->reloadThis("Brands/index"); ?>;
                }
            });
        }
    }
}

</script>
