# Taller Parcial Elect. Desarrollo Web 2017-2
## Hoja de retroalimentación y calificación

### Estudiantes:

> Jhonatan Zuluaga
 
> Nicolas Reyes

> Carlos Cortéz

#
#
Recodemos que...

```
El taller será desarrollado y enregado en grupos mediante la adición del docente al repositorio con permisos de Administración.

El taller busca reforzar sus competencias prácticas en desarrollo web, cubriendo temas de programación orientada a objetos, patrónes de diseño, backend web.
```

## Funcionalidades del sistema

#### Bronce

1. Se respetará el patrón arquitectonico MVC y cada elemento estará ubicado en el lugar supuesto para el mismo.

	**Alcanzado (Si - No):** Si
	
	***
	
2. Usted teniendo ámplios conocimientos en bases de datos va a desarrollar un modelo de datos orientado a objetos, en donde cada tabla equivale a un modelo del proyecto. Su modelo de datos debe soportar como mínimo:
    + Marcas de productos
    + Productos
    + Categorias de productos
    + Proveedores de productos
    + Clientes
    + Carritos de compra
    + Facturas
    + Usuarios
    + Roles de usuarios
    + Menús
    + Comentarios de los productos
    + Calificación de los productos
    + Descuentos en productos
    + Promociones al estilo Bundles, que permiten comprar más barato un combo de productos siempre que se lleven todos al tiempo.

	(Seamos realistas, la mayoría ya los hicimos en clase dentro del modelo de datos).


	**Alcanzado (Si - No):** Si
	
	***
	

3. El proyecto debe contener:
    + al menos 4 usuarios de distintos roles.
    + al menos 3 clientes.
    + al menos 5 categorías.
    + al menos 15 productos por categoría.
    + al menos 5 productos con descuento.
    + al menos 4 promociones tipo bundle.
    + al menos 1 tipo de menú para el panel adminsitrativo por cada rol (es decir al menos 4 tipos de menú).
    + al menos 3 facturas de compras generadas.
    + los productos deben estár totalmente diligenciados y al menos 10% de los productos ya están puntuados o tienen comentarios.

	**Nota:** Use datos que simulen la realidad, todo producto como: Cubo rubik, que contenga una foto de una lámpara o una foto distorcionada, será anulado.


	**Alcanzado (Si - No):** Si
	
	***
	
4. Tanto la tienda como el panel tienen un aspecto comercial, por ende no se aceptarán sitios en construcción ni llenas de textos sin mas. usted podrá usar templates para la tienda si así lo desea como [begge](http://begge.themesun.com/).


	**Alcanzado (Si - No):** Si
	
	***
	
5. las transacciones y relaciones entre los elementos se darán usando ORM, por ende no se aceptarán consultas escritas a mano (no automatizadas) para interactuar con la base de datos.


	**Alcanzado (Si - No):** Si
	
	***
	
### Plata

### General

1. Implemente adecuadamente los patrones de diseño y respete los actuales dentro del proyecto, lo que le implica crear fabricas de objetos por ejemplo.


	**Valor obtenido (0 - 1):** 0.5
	
	***
	
2. Todas las contraseñas deben ser encriptadas.


	**Valor obtenido (0 - 1):** 1
	
	***
	
### Panel Administrativo

1. Todas las entidades del sistema deben poder ser gestionadas desde el panel administrativo, lo que quiere decir que todas las entidades podrán ser creadas, editadas, consultadas y borradas desde el panel. Todo ello mediante el uso de formularios orientados a usuarios finales que **no conocen de ids ni códigos y por eso siempre deben poder ver los nombres asociados a las cosas**.


	**Valor obtenido (0 - 1):** 1
	
	***
	
2. Cada rol tendrá asociada una pag. de inicio que contrandrá un dashboard de información relevante al mismo, por ejemplo, si el rol es de vendedor, el dashboard debería mostrar información de las ventas, productos más vendidos, productos por agotarse, etc. Sea creativo y recuerde que según **bronce** al menos deberá tener 4 dashboards.


	**Valor obtenido (0 - 1):** 1
	
	***
	
3. Todas las consultas serán asincronas, en el caso de tener que subir archivos usted podrá hacerlo usando AJAX2 o falseando la petición con iframes ocultos para conservar la percepción del usuario de asincronismo. Asi mismo, todas las transacciones tendrán respuesta explicita, por ejemplo si un producto es creado, eso debe ser notificado en pantalla, si el producto presentó errores en la creación también y estos deben ser adecuados a los usuarios... no se admitirán mensajes a usuarios como " 1049: Unknown database 'bd_inexistente'" y "1146: Table 'kossu.tabla_inexistente' doesn't exist".


	**Valor obtenido (0 - 1):** 1
	
	***
	
4. El orden de los menus en el panel administrativo podrá ser configurado por un SuperAdmin para cada rol.


	**Valor obtenido (0 - 1):** 0.5
	
	***
	
5. Todos los usuarios pueden hacer cambio de sus datos desde la sección "perfil" del panel administrativo (útil para cambiar contraseñas y actualizar correos.)


	**Valor obtenido (0 - 1):** 1
	
	***
	
### Tienda

1. La tienda cuenta con una página inicial en donde deben aparecer las promociones, los nuevos productos y los bundles, todos ellos destacados sobre los productos en general.


	**Valor obtenido (0 - 1):** 1
	
	***
	
2. La tienda cuenta con buscador de productos que permite realizar la busqueda por marca, categoría y nombre; busquedas que podrán ser ordenadas por:
    + precio (mayor, menor, entre).
    + Comentarios
    + Valoraciones
    + Vendidos


	**Valor obtenido (0 - 1):**  0
	
	***
	
3. El usuario podrá agregar artículos a su carrito, los cuales estarán permanentemente allí hasta que el producto se agote, el usuario los remueva, la cuenta se elimine o se concrete la compra.


	**Valor obtenido (0 - 1):** 1
	
	***
	
4. Al concretar una compra la tienda genera un PDF con la factura para que el usuario la pueda guardar o imprimir.


	**Valor obtenido (0 - 1):** 0.5
	
	***
	
5. Los usuarios podrán crear cuentas para poder realizar compras y a su vez editar su información desde su perfil.

	**Nota:** Se le recomienda pensar en las secciones como templates de información dinámicos, es decir, su tienda debería contemplar las siguientes vistas:
    + Home
    + Search
    + Product
    + ShoppingCart
    + Receipt
    + Profile

	Y por ejemplo la vista Product podría recibir con parametros por URL (GET) el id del producto a visualziar y de esta forma este se mostraría.

	Recuerde también que se asume que el usuario tiene saldo en su cuenta y no existirán validaciones de ese tipo al comprar, mientras que las validaciones de stock en inventario y descuentos si deben llevarse a cabo.

	**Valor obtenido (0 - 1):** 1
	
	***
	

### Adicional

1. La tienda es Responsive. (+0.5)

	**Valor obtenido (0 - 1):** 1
	
	***
	

2. La tienda sugiere a los usuarios productos que otros usuarios han comprado cuando compran ese producto, como lo hace amazon. (+0.5).

	**Valor obtenido (0 - 1):** 1
	
	***
	

3. El panel administrativo lleva un registro de todas las acciones realizadas por los usuarios que se logeen para ser monitoreados por los superadmins y admins. (+0.5)

	**Valor obtenido (0 - 1):**
	
	***
	

### Nota parcial ( 11.5 / 15 ): 3.8
### Bonificaciones: + 0.7
## Nota Final: 4.5